use wgpu::{
    VertexBufferDescriptor,
    InputStepMode,
    VertexAttributeDescriptor,
    VertexFormat,
    BufferAddress,
    ShaderLocation,
    BindGroupLayoutEntry,
    BindGroupLayoutDescriptor,
};
use std::mem::size_of;

use crate::{interface::colors::Color, geom::Location};
use super::objects::{Uniforms, Instance};

pub trait VertexBufferItem<const LOC: ShaderLocation, const STEP: InputStepMode> {
    const BUFFER_DESCRIPTOR: VertexBufferDescriptor<'static>;
}

impl<const LOC: ShaderLocation, const STEP: InputStepMode>
VertexBufferItem<{LOC}, {STEP}>
for Location
{
    const BUFFER_DESCRIPTOR: VertexBufferDescriptor<'static> = VertexBufferDescriptor {
        stride: size_of::<Self>() as BufferAddress,
        step_mode: STEP,
        attributes: &[
            VertexAttributeDescriptor {
                offset: 0,
                format: VertexFormat::Float2,
                shader_location: LOC,
            },
        ],
    };
}

impl<const LOC: ShaderLocation>
VertexBufferItem<{LOC}, {InputStepMode::Instance}>
for Instance
{
    const BUFFER_DESCRIPTOR: VertexBufferDescriptor<'static> = VertexBufferDescriptor {
        stride: size_of::<Self>() as BufferAddress,
        step_mode: InputStepMode::Instance,
        attributes: &[
            VertexAttributeDescriptor {
                offset: 0,
                format: VertexFormat::Float4,
                shader_location: LOC,
            },
            VertexAttributeDescriptor {
                offset: size_of::<Color>() as BufferAddress,
                format: VertexFormat::Float2,
                shader_location: LOC + 1,
            },
            VertexAttributeDescriptor {
                offset: (
                    size_of::<Color>() +
                    size_of::<Location>()
                ) as BufferAddress,
                format: VertexFormat::Float,
                shader_location: LOC + 2,
            },
        ],
    };
}


pub trait BindGroupItem {
    const BIND_GROUP_LAYOUT_DESCRIPTOR: BindGroupLayoutDescriptor<'static>;
}

impl BindGroupItem for Uniforms {
    const BIND_GROUP_LAYOUT_DESCRIPTOR: BindGroupLayoutDescriptor<'static> = BindGroupLayoutDescriptor {
        label: None,
        bindings: &[
            BindGroupLayoutEntry {
                binding: 0,
                visibility: wgpu::ShaderStage::VERTEX,
                ty: wgpu::BindingType::UniformBuffer {
                    dynamic: false,
                },
            },
        ],
    };
}
