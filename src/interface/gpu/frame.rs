use wgpu::{CommandEncoder, RenderPass, BindGroup, RenderPipeline, TextureView};

use crate::{
    interface::{
        BACKGROUND_COLOR,
        gpu::{
            Gpu,
            buffer::proxy::{InstancedMeshProxy, Models},
        },
    },
    board::Selection,
};

pub fn render(
    gpu: &mut Gpu,
    uniforms: &BindGroup,
    vertex: &Models,
    selection: Option<Selection>,
) {
    let frame = gpu.next_frame();
    let mut command = gpu.command_encoder();

    {
        let mut pass = Pass::new(
            &mut command,
            &frame.view,
            &gpu.render_pipeline,
            &uniforms,
        );

        pass.indexed_mesh(&vertex.tiles);
        pass.indexed_mesh(&vertex.roads);
        pass.indexed_mesh(&vertex.settlements);
        pass.indexed_mesh(&vertex.cities);
        pass.hover(&vertex, selection);
    }

    gpu.submit(command.finish())
}

pub struct Pass<'a>(RenderPass<'a>);

impl<'rend, 'data: 'rend> Pass<'rend> {
    fn new(
        command: &'rend mut CommandEncoder,
        view: &'rend TextureView,
        pipeline: &'rend RenderPipeline,
        uniforms: &'data BindGroup,
    ) -> Self {
        let mut pass = command.begin_render_pass(&wgpu::RenderPassDescriptor {
            color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                attachment: view,
                resolve_target: None,
                load_op: wgpu::LoadOp::Clear,
                store_op: wgpu::StoreOp::Store,
                clear_color: BACKGROUND_COLOR,
            }],
            depth_stencil_attachment: None,
        });

        pass.set_pipeline(&pipeline);
        pass.set_bind_group(0, &uniforms, &[]);

        Self(pass)
    }

    pub fn indexed_mesh(&mut self, InstancedMeshProxy { mesh, instances}: &'data InstancedMeshProxy) {
        self.0.set_vertex_buffer(0, &mesh.vertex_buffer, 0, 0);
        self.0.set_index_buffer (   &mesh.index_buffer,  0, 0);
        self.0.set_vertex_buffer(1, &instances,          0, 0);

        self.0.draw_indexed(mesh.range(), 0, instances.range());
    }

    #[allow(dead_code)]
    pub fn hover(&mut self, vertex_buffers: &'data Models, selection: Option<Selection>) {
        let hour = match selection {
            Some(selection) => selection.hour,
            None => return,
        };

        let mesh = match hour {
            Some(hour) if hour.is_side() => &vertex_buffers.roads.mesh,
            _ => &vertex_buffers.hover_circle,
        };

        self.0.set_vertex_buffer(0, &mesh.vertex_buffer,   0, 0);
        self.0.set_index_buffer (   &mesh.index_buffer,    0, 0);
        self.0.set_vertex_buffer(1, &vertex_buffers.hover, 0, 0);

        self.0.draw_indexed(mesh.range(), 0, vertex_buffers.hover.range());
    }
}

// # Tokens
/*
let num_text = dice_value.to_str();
let num_size = renderer.tile_side_length as Scalar / TOKEN_FONT_FRAC;
let num_pos = location.trans(
    -(num_size * num_text.len() as Scalar * 0.28), // Center X
    num_size as Scalar / 2.5, // Center Y
);

graphics::ellipse(colors::TOKEN, shapes::TOKEN, hex_transform, renderer.gfx);
graphics::text::Text::new((num_size * TOKEN_FONT_SSAA) as FontSize)
    .round()
    .draw(num_text, renderer.glyph_cache, &renderer.ctx.draw_state, num_pos.zoom(TOKEN_FONT_SSAA.recip()), renderer.gfx)
    .expect("Fatal glyph cache error")
*/

// # Harbor
/*
use graphics::ellipse::{Ellipse, Border};

Ellipse::new(colors::TRANSPARENT)
    .border(Border { color: colors::HARBOR, radius: shapes::HARBOR_BORDER })
    .draw(shapes::HARBOR, &renderer.ctx.draw_state, hex_transform, renderer.gfx);

if let Harbor::Special(resource) = harbor {
    graphics::ellipse(resource.into(), shapes::HARBOR_INSIDE, hex_transform, renderer.gfx)
}
*/
