use crate::{
    interface::BOARD_MARGINS,
    geom::{
        shapes::{RADIUS_TO_DIAMETER, hex, TILE_SPACING},
        Scalar,
        hour::Hour,
        Vector,
        Location,
        Geom2Ext,
    },
    board::{Selection, Board},
};
use hex2d::Coordinate;
use cgmath::{Vector2, ElementWise, EuclideanSpace, InnerSpace};
use winit::dpi::PhysicalSize;

#[derive(Debug)]
pub struct ViewLayer {
    viewport: Vector,
    tile_side_length: Scalar,
}

impl ViewLayer {
    pub fn new(board: &Board, viewport: PhysicalSize<u32>) -> Self {
        let PhysicalSize { width, height } = viewport;
        let viewport = Vector2::new(width as Scalar, height as Scalar);

        let tile_side_length = Self::compute_tile_side_length(viewport, board);

        Self {
            viewport,
            tile_side_length,
        }
    }

    pub fn resize_viewport(&mut self, board: &Board, new_size: Vector) {
        self.viewport = new_size;
        self.tile_side_length = Self::compute_tile_side_length(new_size, board);
    }

    fn compute_tile_side_length(viewport: Vector, board: &Board) -> Scalar {
        let Coordinate { x, y } = board.bounds().span();
        let board_coord_span = Vector2 { x, y }.cast::<Scalar>().unwrap().map(|n| n + 1.0);
        let board_span = board_coord_span
            .mul_element_wise(hex::GRID_ASPECT)
            .mul_element_wise(RADIUS_TO_DIAMETER);

        viewport
            .mul_element_wise(BOARD_MARGINS)
            .div_element_wise(board_span)
            .lesser()
    }

    pub fn viewport(&self) -> Vector { self.viewport }
    // pub fn tile_side_length(&self) -> Scalar { self.tile_side_length }
    pub fn viewport_scale(&self) -> Vector {
        self.tile_side_length * RADIUS_TO_DIAMETER / self.viewport
    }

    pub fn compute_selection(&self, board: &Board, mouse_absolute: Location) -> Option<Selection> {
        let mouse = mouse_absolute - (self.viewport / 2.0);

        let coord = Coordinate::from_pixel(
            mouse.x,
            mouse.y,
            TILE_SPACING(self.tile_side_length as f32),
        );

        if !board.contains_tile(coord) { return None; }

        let tile_loc = coord.to_pixel(TILE_SPACING(self.tile_side_length as f32));
        let local_mouse = mouse - Vector2::from(tile_loc);

        let center_radius = self.tile_side_length / 2.0;
        let select_is_center = local_mouse.to_vec().magnitude() <= center_radius;

        let hour = if select_is_center {
            None
        } else {
            Some(Hour::from(local_mouse.to_vec().angle(Vector2::unit_x())))
        };

        Some(Selection { coord, hour })
    }
}
