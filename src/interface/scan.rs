use crate::{geom::{shapes::{STRAIGHT, TILE_SPACING}, located::Located, CoordinateExt}, board::{Board, piece::{TownKind, Road}}};
use super::{colors::Color, gpu::buffer::objects::Instance};
use cgmath::Rad;

#[derive(Clone, Copy)]
pub struct Instancer<'a>(pub &'a Board);

impl<'a> Instancer<'a> {
    pub fn tiles(self) -> Vec<Instance> {
        self.0.tiles()
            .map(|Located(coord, tile)| Instance {
                color: tile.kind.into(),
                location: coord.to_cartesian(TILE_SPACING),
                rotation: Rad(0.0),
            })
            .collect()
    }

    pub fn towns(self, kind: TownKind) -> Vec<Instance> {
        self.0.tiles()
            .filter_map(|Located(coord, tile)| tile
                .town()
                .filter(|(town, _)| town.kind == kind)
                .map(move |(town, side)| Instance {
                    color: town.color.into(),
                    location: side.location(coord),
                    rotation: STRAIGHT,
                })
            )
            .collect()
    }

    pub fn roads(self) -> Vec<Instance> {
        self.0.tiles()
            .flat_map(|Located(coord, tile)| tile
                .roads()
                .map(move |(index, Road(color))| Instance {
                    color: color.into(),
                    location: index.location(coord),
                    rotation: index.tilt(),
                })
            )
            .collect()
    }

    pub fn robbers(self) -> Vec<Instance> {
        self.0.tiles()
            .filter(|Located(_, tile)| tile.robber.is_some())
            .map(|Located(coord, _)| Instance {
                color: Color::ROBBER,
                location: coord.to_cartesian(TILE_SPACING),
                rotation: STRAIGHT,
            })
            .collect()
    }
}
