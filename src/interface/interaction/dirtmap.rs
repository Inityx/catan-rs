#[derive(Debug, PartialEq, Default)]
pub struct Dirtmapped<T> {
    inner: T,
    dirty: bool,
}

impl<T> Dirtmapped<T> {
    #[allow(dead_code)]
    pub const fn dirty(inner: T) -> Self {
        Self { inner, dirty: true }
    }

    pub const fn clean(inner: T) -> Self {
        Self { inner, dirty: false }
    }

    pub fn get(&self) -> &T {
        &self.inner
    }

    pub fn write(&mut self, new: T) {
        self.dirty = true;
        self.inner = new;
    }

    pub fn modify<U>(&mut self, f: impl FnOnce(&mut T) -> U) -> U {
        self.dirty = true;
        f(&mut self.inner)
    }

    pub fn sync<U>(&mut self, f: impl FnOnce(&T) -> U) -> Option<U> {
        if self.dirty {
            self.dirty = false;
            Some(f(&mut self.inner))
        } else {
            None
        }
    }
}
