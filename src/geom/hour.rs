use std::f32::consts::PI;
use crate::geom::{Scalar, shapes::hex};
use super::{Rotation, Vector};
use cgmath::Rad;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Hour { H12 = 0, H1, H2, H3, H4, H5, H6, H7, H8, H9, H10, H11 }

impl Hour {
    const DOMAIN: Scalar = PI * 2.0;
    const RANGE: usize = 12;

    pub const ALL: [Hour; Self::RANGE] = [
        Hour::H12,
        Hour::H1,
        Hour::H2,
        Hour::H3,
        Hour::H4,
        Hour::H5,
        Hour::H6,
        Hour::H7,
        Hour::H8,
        Hour::H9,
        Hour::H10,
        Hour::H11,
    ];

    const OFFSETS: [Vector; Self::RANGE] = geom_pairs![
        Vector,
        [-hex::MINOR,        hex::ORIGIN     ], // Left
        [-hex::MINOR,        hex::MAJOR / 2.0],
        [-hex::MINOR / 2.0,  hex::DIAG_Y     ],
        [ hex::ORIGIN,       hex::MAJOR      ], // Top
        [ hex::MINOR / 2.0,  hex::DIAG_Y     ],
        [ hex::MINOR,        hex::MAJOR / 2.0],
        [ hex::MINOR,        hex::ORIGIN     ], // Right
        [ hex::MINOR,       -hex::MAJOR / 2.0],
        [ hex::MINOR / 2.0, -hex::DIAG_Y     ],
        [ hex::ORIGIN,      -hex::MAJOR      ], // Bottom
        [-hex::MINOR / 2.0, -hex::DIAG_Y     ],
        [-hex::MINOR,       -hex::MAJOR / 2.0],
    ];

    /// H6 is 0 radians
    pub const fn rotation(self) -> Rotation {
        let hour_val = self as usize as Scalar;
        let stretched = hour_val * (Self::DOMAIN / Self::RANGE as Scalar);
        let centered = stretched - PI;
        Rad(-centered)
    }

    /// H6 is the right side, hexagon major radius is 1
    pub const fn displacement(self) -> Vector {
        Self::OFFSETS[self as usize]
    }

    pub const fn is_side(self) -> bool {
        self as usize % 2 == 0
    }

    // pub const fn is_corner(self) -> bool {
    //     !self.is_side()
    // }
}

impl From<Rotation> for Hour {
    /// H6 is 0 radians
    fn from(Rad(radians): Rotation) -> Self {
        let inverted = -radians;
        let positive = inverted + PI;
        let aligned = (positive + PI / Self::RANGE as Scalar / 2.0) % Self::DOMAIN;
        let stretched = aligned * (Self::RANGE as Scalar / Self::DOMAIN);
        Self::ALL[stretched as usize]
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use cgmath::assert_ulps_eq;

    // twelfths in order of Hour::ALL
    const TWELFTHS: [Scalar;12] = [6.0, 5.0, 4.0, 3.0, 2.0, 1.0, 0.0, -1.0, -2.0, -3.0, -4.0, -5.0];

    fn twelfths() -> impl Iterator<Item=Rotation> {
        TWELFTHS.iter().cloned().map(twelfth_angle)
    }

    fn twelfth_angle(twelfth: Scalar) -> Rotation {
        Rad(PI / 6.0 * twelfth as Scalar)
    }

    #[test]
    fn to_angle() {
        let computed_twelfths: Vec<_> = Hour::ALL.iter().cloned().map(Hour::rotation).collect();

        for (computed, original) in computed_twelfths.into_iter().zip(twelfths()) {
            assert_ulps_eq!(original, computed);
        }
    }

    #[test]
    fn from_angle_exact() {
        let computed_hours: Vec<_> = twelfths().map(Hour::from).collect();

        assert_eq!(Hour::ALL, computed_hours.as_slice());
    }

    #[test]
    fn from_between() {
        let source = vec![
            ( 5.8, Hour::H12),
            ( 5.2, Hour::H1 ),
            ( 4.8, Hour::H1 ),
            ( 4.2, Hour::H2 ),
            ( 3.8, Hour::H2 ),
            ( 3.2, Hour::H3 ),
            ( 2.8, Hour::H3 ),
            ( 2.2, Hour::H4 ),
            ( 1.8, Hour::H4 ),
            ( 1.2, Hour::H5 ),
            ( 0.8, Hour::H5 ),
            ( 0.2, Hour::H6 ),
            (-0.2, Hour::H6 ),
            (-0.8, Hour::H7 ),
            (-1.2, Hour::H7 ),
            (-1.8, Hour::H8 ),
            (-2.2, Hour::H8 ),
            (-2.8, Hour::H9 ),
            (-3.2, Hour::H9 ),
            (-3.8, Hour::H10),
            (-4.2, Hour::H10),
            (-4.8, Hour::H11),
            (-5.2, Hour::H11),
            (-5.8, Hour::H12),
        ];

        for (twelfth, hour) in source {
            let computed = Hour::from(twelfth_angle(twelfth));
            assert!(
                hour == computed,
                "Twelfth {} produced {:?} instead of {:?}",
                twelfth, computed, hour,
            );
        }
    }
}
