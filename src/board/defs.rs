#![allow(dead_code)]
use std::iter::repeat_with;
use rand::random;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Resource {
    Brick,
    Lumber,
    Ore,
    Grain,
    Wool,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum DiceValue { D2, D3, D4, D5, D6, D7, D8, D9, D10, D11, D12 }

impl DiceValue {
    pub const ALL: [Self; 11] = {
        use DiceValue::*;
        [D2, D3, D4, D5, D6, D7, D8, D9, D10, D11, D12]
    };

    fn roll() -> Self {
        let index = repeat_with(random::<usize>)
            .map(|x| x % 6)
            .take(2)
            .sum::<usize>();

        Self::ALL[index]
    }

    pub const fn to_str(self) -> &'static str {
        use DiceValue::*;
        match self {
            D2  => "2",
            D3  => "3",
            D4  => "4",
            D5  => "5",
            D6  => "6",
            D7  => "7",
            D8  => "8",
            D9  => "9",
            D10 => "10",
            D11 => "11",
            D12 => "12",
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum PlayerColor {
    Blue,
    Red,
    Green,
    White,
    Orange,
    Brown,
}

impl PlayerColor {
    pub const ALL: [PlayerColor; 6] = [
        PlayerColor::Blue,
        PlayerColor::Red,
        PlayerColor::Green,
        PlayerColor::White,
        PlayerColor::Orange,
        PlayerColor::Brown,
    ];
}
