#![warn(clippy::all)]
#![allow(incomplete_features)]
#![feature(
    try_trait,
    trait_alias,
    const_generics,
    fixed_size_array,
    array_value_iter,
    move_ref_pattern,
    const_fn,
    core_intrinsics,
    bindings_after_at,
)]

#[macro_use]
mod geom;

mod board;
mod interface;

use crate::{
    board::{
        Selection,
        defs::PlayerColor,
        piece::{Road, Town, TownKind},
    },
    interface::controller::sandbox::Controller as Sandbox,
    geom::hour::Hour,
};

use hex2d::Coordinate;
use board::Board;

fn main() {
    let mut board = Board::standard_layout();
    let roads = std::array::IntoIter::new([
        ((0,  0), Hour::H2),
        ((0,  0), Hour::H4),
        ((1, -1), Hour::H2),
    ]);
    let towns = std::array::IntoIter::new([
        ((0,  0), Hour::H1, TownKind::Settlement),
        ((0,  0), Hour::H5, TownKind::City),
    ]);

    for ((x, y), hour) in roads {
        board.try_put(
            Road(PlayerColor::Green),
            Selection { coord: Coordinate { x, y }, hour: Some(hour) },
        ).unwrap();
    }

    for ((x, y), hour, kind) in towns {
        board.try_put(
            Town { kind, color: PlayerColor::Green },
            Selection { coord: Coordinate { x, y }, hour: Some(hour) },
        ).unwrap();
    }

    Sandbox::play(board);
}
