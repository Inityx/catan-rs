use winit::{
    event::{ElementState, VirtualKeyCode, MouseButton, WindowEvent, KeyboardInput},
    dpi::PhysicalPosition,
};
use crate::geom::{Scalar, Location};

#[derive(Default)]
pub struct InputLayer {
    pub ctrl: bool,
    pub last_key: Option<VirtualKeyCode>,
    pub cursor: Option<Location>,
    pub click: Option<(MouseButton, Location)>,
}

impl InputLayer {
    pub fn accept(&mut self, event: WindowEvent) -> bool {
        match event {
            WindowEvent::ModifiersChanged(mods) => self.ctrl = mods.ctrl(),
            WindowEvent::KeyboardInput {
                input: KeyboardInput {
                    virtual_keycode: Some(keycode),
                    state: ElementState::Pressed,
                    ..
                },
                ..
            } => self.last_key = Some(keycode),

            WindowEvent::MouseInput {
                button,
                state: ElementState::Released,
                ..
            } => if let Some(position) = self.cursor {
                self.click = Some((button, position));
            },

            WindowEvent::CursorMoved {
                position: PhysicalPosition { x, y },
                ..
            } => self.cursor = Some(Location::new(x as Scalar, y as Scalar)),

            WindowEvent::CursorLeft { .. } => self.cursor = None,

            _ => return false,
        }

        true
    }
}
