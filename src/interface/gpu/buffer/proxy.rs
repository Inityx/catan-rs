use wgpu::{BufferUsage, BindGroup, Binding, Device};
use crate::{interface::gpu::Gpu, geom::{mesh::{self, RefMesh}, Location, VertIndex}};
use std::ops::Range;
use super::{objects::{Uniforms, Instance}, collections::{BufferVec, BufferOption, BufferValue}};

pub struct Models {
    pub tiles: InstancedMeshProxy,
    pub roads: InstancedMeshProxy,
    pub settlements: InstancedMeshProxy,
    pub cities: InstancedMeshProxy,
    pub robbers: InstancedMeshProxy,
    pub hover_circle: MeshProxy,
    pub hover: BufferOption<Instance>,
}

impl Models {
    pub fn new(
        device: &Device,
        tiles: &[Instance],
        roads: &[Instance],
        settlements: &[Instance],
        cities: &[Instance],
        robbers: &[Instance],
    ) -> Self {
        Self {
            tiles:        InstancedMeshProxy::new(device, mesh::HEXAGON.to_mesh().as_ref_mesh(),    tiles),
            roads:        InstancedMeshProxy::new(device, mesh::ROAD,       roads),
            settlements:  InstancedMeshProxy::new(device, mesh::SETTLEMENT, settlements),
            cities:       InstancedMeshProxy::new(device, mesh::CITY,       cities),
            robbers:      InstancedMeshProxy::new(device, mesh::ROBBER,     robbers),
            hover_circle: MeshProxy::new(device, mesh::HOVER_CIRCLE.to_mesh().as_ref_mesh()),
            hover:        BufferOption::new(device, None, BufferUsage::VERTEX | BufferUsage::MAP_WRITE),
        }
    }
}

pub struct InstancedMeshProxy {
    pub mesh: MeshProxy,
    pub instances: BufferVec<Instance>,
}

impl InstancedMeshProxy {
    pub fn new(device: &Device, mesh: RefMesh, instances: &[Instance]) -> Self {
        Self {
            mesh: MeshProxy::new(device, mesh),
            instances: BufferVec::new(device, instances, BufferUsage::VERTEX | BufferUsage::MAP_WRITE),
        }
    }
}

pub struct MeshProxy {
    pub vertex_buffer: BufferVec<Location>,
    pub index_buffer: BufferVec<VertIndex>,
}

impl MeshProxy {
    pub fn new(device: &Device, mesh: RefMesh) -> Self {
        Self {
            vertex_buffer: BufferVec::new(&device, mesh.vertices, BufferUsage::VERTEX),
            index_buffer:  BufferVec::new(&device, mesh.indices , BufferUsage::INDEX ),
        }
    }

    /// Range of indexes to draw
    pub fn range(&self) -> Range<u32> {
        0..self.index_buffer.len() as u32
    }
}

pub struct UniformBufferProxy {
    pub value: BufferValue<Uniforms>,
    pub bind_group: BindGroup,
}

impl UniformBufferProxy {
    pub fn new(gpu: &Gpu, initial: Uniforms) -> Self {
        let value = BufferValue::new(
            &gpu.device,
            initial,
            BufferUsage::UNIFORM,
        );

        let bind_group = gpu.bind_group(
            &gpu.uniform_bind_group_layout,
            &[Binding {
                binding: 0,
                resource: value.binding_resource(),
            }],
        );

        Self { value, bind_group }
    }
}
