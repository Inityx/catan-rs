
use wgpu::{BufferAddress, BufferUsage, Buffer, Device, BufferDescriptor, Maintain, BindingResource};
use std::{mem::size_of, marker::PhantomData, ops::{Deref, DerefMut}, slice};
use futures::executor::block_on;
use bytemuck::Pod;
use super::AsRawBytes;
use crate::interface::gpu::DrawRange;

/// Host-side handle managing value-like GPU buffer allocations
pub struct BufferValue<T> {
    buffer: Buffer,
    phantom: PhantomData<T>,
}

impl<T: Pod> BufferValue<T> {
    const ITEM_SIZE: BufferAddress = size_of::<T>() as BufferAddress;

    pub fn new(device: &Device, value: T, usage: BufferUsage) -> Self {
        Self {
            buffer: device.create_buffer_with_data(bytemuck::bytes_of(&value), usage),
            phantom: PhantomData,
        }
    }

    // pub fn set(&mut self, device: &Device, value: T) {
    //     self.modify(device, |item| *item = value);
    // }

    pub fn modify(&mut self, device: &Device, change: impl FnOnce(&mut T)) {
        let mapping = self.buffer.map_write(0, Self::ITEM_SIZE);
        device.poll(Maintain::Wait);
        let mut mapping = block_on(mapping).unwrap();
        let gpu_data = bytemuck::from_bytes_mut(mapping.as_slice());

        change(gpu_data);
    }

    pub fn binding_resource(&self) -> BindingResource {
        BindingResource::Buffer {
            buffer: &self.buffer,
            range: 0..Self::ITEM_SIZE as BufferAddress,
        }
    }
}

impl<T> Deref for BufferValue<T> {
    type Target = Buffer;
    fn deref(&self) -> &Self::Target { &self.buffer }
}

impl<T> DerefMut for BufferValue<T> {
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.buffer }
}

/// Host-side handle managing Vec-like GPU buffer allocations
pub struct BufferVec<T> {
    buffer: Buffer,
    count: usize,
    capacity: usize,
    usage: BufferUsage,
    phantom: PhantomData<T>,
}

impl<T: 'static> BufferVec<T>
where [T]: AsRawBytes
{
    const ITEM_SIZE: BufferAddress = size_of::<T>() as BufferAddress;

    pub fn new(device: &Device, data: &[T], usage: BufferUsage) -> Self {
        if data.is_empty() {
            Self::empty(device, usage)
        } else {
            Self::from_raw_parts(
                device.create_buffer_with_data(data.as_raw_bytes(), usage),
                usage,
                data.len(),
            )
        }
    }

    fn empty(device: &Device, usage: BufferUsage) -> Self {
        Self {
            buffer: device.create_buffer(&BufferDescriptor {
                size: Self::ITEM_SIZE,
                usage,
                label: None,
            }),
            count: 0,
            capacity: 1,
            usage,
            phantom: PhantomData,
        }
    }

    fn from_raw_parts(buffer: Buffer, usage: BufferUsage, count: usize) -> Self {
        Self {
            buffer,
            count,
            capacity: count,
            usage,
            phantom: PhantomData,
        }
    }

    pub fn len(&self) -> usize {
        self.count
    }

    pub fn is_empty(&self) -> bool {
        self.count == 0
    }

    pub fn range(&self) -> DrawRange {
        0..self.count as u32
    }

    pub fn size_in_bytes(&self) -> BufferAddress {
        self.count as BufferAddress * Self::ITEM_SIZE
    }

    fn capacity_in_bytes(&self) -> BufferAddress {
        self.capacity as BufferAddress * Self::ITEM_SIZE
    }

    pub fn replace(&mut self, device: &Device, data: &[T]) {
        debug_assert!(self.capacity > 0);

        let too_small = self.capacity < data.len();
        let too_large = self.capacity - 1 > data.len() * 4;

        if too_small || too_large {
            self.capacity = data.len() * 2;
            self.buffer = device.create_buffer(&BufferDescriptor {
                size: self.capacity_in_bytes(),
                usage: self.usage,
                label: None,
            });
        } else {
            self.count = data.len();
        }

        if self.is_empty() { return; }

        let mapping = self.buffer.map_write(0, self.size_in_bytes());
        device.poll(Maintain::Wait);
        let mut mapping = block_on(mapping).unwrap();
        let gpu_data = bytemuck::cast_slice_mut(mapping.as_slice());

        gpu_data.copy_from_slice(data.as_raw_bytes());
    }
}

impl<T> Deref for BufferVec<T> {
    type Target = Buffer;
    fn deref(&self) -> &Self::Target { &self.buffer }
}

impl<T> DerefMut for BufferVec<T> {
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.buffer }
}

/// Host-side handle managing Option-like GPU buffer allocations
pub struct BufferOption<T>(BufferVec<T>);

impl<T: 'static> BufferOption<T>
where [T]: AsRawBytes
{
    pub fn new(device: &Device, value: Option<T>, usage: BufferUsage) -> Self {
        Self(BufferVec::new(
            device,
            Self::option_slice(value.as_ref()),
            usage,
        ))
    }

    pub fn is_none(&self) -> bool {
        self.0.is_empty()
    }

    // pub fn is_some(&self) -> bool {
    //     !self.is_none()
    // }

    pub fn replace(&mut self, device: &Device, value: Option<T>) {
        if value.is_none() && self.is_none() { return; }
        self.0.replace(device, Self::option_slice(value.as_ref()));
    }

    pub fn range(&self) -> DrawRange {
        self.0.range()
    }

    #[inline(always)]
    fn option_slice(option: Option<&T>) -> &[T] {
        option.map(slice::from_ref).unwrap_or(&[])
    }
}

impl<T> Deref for BufferOption<T> {
    type Target = Buffer;
    fn deref(&self) -> &Self::Target { &*self.0 }
}

impl<T> DerefMut for BufferOption<T> {
    fn deref_mut(&mut self) -> &mut Self::Target { &mut *self.0 }
}
