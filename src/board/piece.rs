#![allow(dead_code)]
use crate::board::defs::{PlayerColor, Resource};

#[derive(Debug, Clone)]
pub enum Piece {
    Town(Town),
    Road(Road),
    Robber(Robber),
}

impl From<Town  > for Piece { fn from(t: Town  ) -> Self { Self::Town  (t) } }
impl From<Road  > for Piece { fn from(r: Road  ) -> Self { Self::Road  (r) } }
impl From<Robber> for Piece { fn from(r: Robber) -> Self { Self::Robber(r) } }

#[derive(Debug, Clone, PartialEq)]
pub enum TownKind {
    Settlement,
    City,
}

#[derive(Debug, Clone)]
pub struct Town {
    pub kind: TownKind,
    pub color: PlayerColor,
}

#[derive(Debug, Clone)]
pub struct Road(pub PlayerColor);

#[derive(Debug, Clone)]
pub struct Robber;

#[must_use]
#[derive(Debug, PartialEq)]
pub enum Card {
    Resource(Resource),
    Development(Development),
}

impl Card {
    pub fn is_victory_point(&self) -> bool {
        matches!(self, Self::Development(Development::Victory))
    }
}

impl From<Resource> for Card {
    fn from(resource: Resource) -> Card {
        Card::Resource(resource)
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Development {
    Knight,
    Victory,
    Progress(Progress),
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Progress {
    RoadBuilding,
    YearOfPlenty,
    Monopoly,
}

#[must_use]
#[derive(Debug, PartialEq)]
pub enum Achievement {
    LongestRoad,
    LargestArmy,
}
