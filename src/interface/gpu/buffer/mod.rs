use bytemuck::{Pod, Zeroable};
use crate::geom::{Scalar, Location, VertIndex};
use std::slice;

pub mod proxy;
pub mod descriptor_traits;
pub mod collections;
pub mod objects;

pub trait AsRawBytes {
    type Primitives: Pod + Zeroable;
    fn as_slice_of_primitives(&self) -> &[Self::Primitives];
    fn as_raw_bytes(&self) -> &[u8] { bytemuck::cast_slice(self.as_slice_of_primitives()) }
}

impl<T: AsRawBytes> AsRawBytes for [T] {
    type Primitives = T::Primitives;
    fn as_slice_of_primitives(&self) -> &[Self::Primitives] {
        let self_ptr = self as *const Self;
        let converted = self_ptr as *const [Self::Primitives];
        unsafe { &*converted }
    }
}

impl AsRawBytes for Location {
    type Primitives = [Scalar;2];
    fn as_slice_of_primitives(&self) -> &[Self::Primitives] {
        let primitives = self as *const Self as *const Self::Primitives;
        slice::from_ref(unsafe { &*primitives })
    }
}

macro_rules! auto_raw_bytes {
    ($($t:ty)+) => {
        $(
            impl AsRawBytes for $t {
                type Primitives = $t;
                fn as_slice_of_primitives(&self) -> &[Self::Primitives] { slice::from_ref(self) }
            }
        )+
    };
}

auto_raw_bytes! {
    VertIndex
    objects::Instance
    objects::Uniforms
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn u16() {
        assert_eq!(&[0xCD, 0xAB], (&[0xABCDu16][..]).as_raw_bytes());
    }

    #[test]
    fn vertex() {
        assert_eq!(
            &[
                0x00, 0x00, 0xB0, 0x40,
                0x33, 0x33, 0x13, 0x40,
            ],
            (&[Location::new(5.5, 2.3)][..]).as_raw_bytes(),
        );
    }
}
