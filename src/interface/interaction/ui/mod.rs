use crate::geom::{Geom2Ext, DisplayPair, Location, Scalar, Vector, BoundingBox};
use std::fmt;
use cgmath::{ElementWise, Vector2, EuclideanSpace};

pub mod layout;
pub use layout::{Children, Position};

pub const TOLERANCE: f32 = 0.000001;

#[derive(Debug)]
pub struct Widget<Message> {
    proportion: Vector,
    position: Position,
    children: Children<Message>,
}

impl<Message> Widget<Message> {
    pub fn new(proportion: Vector, position: Position, children: Children<Message>) -> Self {
        // Widgets are theoretically allowed to be off the screen
        Self { proportion, position, children: children.validated() }
    }

    pub fn ray_intersect(&self, screen_size: Vector, location: Location) -> WidgetRay<'_, Message> {
        let location = location.div_element_wise(Location::from_vec(screen_size));
        let self_offset = self.position.child_offset(self.proportion);

        let self_bounding_box = BoundingBox::offset_size(self_offset, self.proportion);
        if !self_bounding_box.contains(location, TOLERANCE) {
            return WidgetRay(None);
        }

        WidgetRay(self.children.probe_at(self_bounding_box.relative(location)))
    }
}

pub struct SubWidget<Message> {
    size: Vector,
    on_click: Option<Message>,
    children: Children<Message>,
}

impl<Message> SubWidget<Message> {
    pub fn new(size: Vector2<Scalar>, on_click: Option<Message>, children: Children<Message>) -> Self {
        assert!(
            size.is_normalized(),
            "SubWidget size should be normalized: {}",
            DisplayPair(size.into()),
        );
        Self { size, on_click, children: children.validated() }
    }
}

impl<Message: fmt::Debug> fmt::Debug for SubWidget<Message> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Widget({}", DisplayPair(self.size.into()))?;
        if self.children.len() > 0 {
            write!(f, ", {} children", self.children.len())?;
        }
        if let Some(message) = self.on_click.as_ref() {
            write!(f, ", on click {:?}", message)?;
        }
        write!(f, ")")
    }
}

pub struct WidgetRay<'a, Message>(
    Option<(Location, &'a SubWidget<Message>)>,
);

impl<'a, Message> Iterator for WidgetRay<'a, Message> {
    type Item = &'a Message;
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let (location, widget) = self.0?;
            self.0 = widget.children.probe_at(location);

            if let Some(ref on_click) = widget.on_click {
                break Some(on_click);
            }
        }
    }
}
