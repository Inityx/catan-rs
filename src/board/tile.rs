use hex2d::{Coordinate, Direction};
use crate::{
    geom::{Rotation, shapes, Location, CoordinateExt, hour::Hour},
    board::{
        defs::{DiceValue, Resource},
        piece::{Robber, Town, Road},
    },
};
use super::piece::Piece;
use shapes::{TILE_SPACING};
use std::ops::Neg;


#[derive(Debug, Clone, Copy)]
pub enum Harbor {
    Generic,
    Special(Resource),
}

impl Harbor {
    // fn requires(self) -> (usize, Option<Resource>) {
    //     match self {
    //         Harbor::Generic           => (3, None          ),
    //         Harbor::Special(resource) => (2, Some(resource)),
    //     }
    // }
}

#[derive(Debug, Clone, Copy)]
pub enum TileKind {
    Land(Resource, DiceValue),
    Water(Option<Harbor>),
    Desert,
}

#[allow(dead_code)]
impl TileKind {
    pub fn is_water (self) -> bool { matches!(self, TileKind::Water(_)  ) }
    pub fn is_desert(self) -> bool { matches!(self, TileKind::Desert    ) }
    pub fn is_land  (self) -> bool { matches!(self, TileKind::Land(_, _)) }
}


#[derive(Clone, Copy, Debug, PartialEq)]
pub enum TownSide { Right, Left }

impl TownSide {
    pub fn hour(self) -> Hour {
        Hour::ALL[7 + (self as usize * 2)]
    }

    pub fn location(self, coord: Coordinate<isize>) -> Location {
        coord.to_cartesian(TILE_SPACING) + self.hour().displacement()
    }
}

impl From<TownSide> for Direction {
    fn from(side: TownSide) -> Direction {
        match side {
            TownSide::Left  => Direction::ZX,
            TownSide::Right => Direction::XY,
        }
    }
}

impl Neg for TownSide {
    type Output = Self;
    fn neg(self) -> Self::Output {
        match self {
            TownSide::Left => TownSide::Right,
            TownSide::Right => TownSide::Left,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum RoadIndex { R0, R1, R2 }

impl RoadIndex {
    pub const COUNT: usize = 3;
    pub const ALL: [RoadIndex; Self::COUNT] = [RoadIndex::R0, RoadIndex::R1, RoadIndex::R2];

    pub fn tilt(self) -> Rotation {
        -self.hour().rotation()
    }

    pub fn location(self, coord: Coordinate<isize>) -> Location {
        coord.to_cartesian(TILE_SPACING) + self.hour().displacement()
    }

    const fn hour(self) -> Hour {
        Hour::ALL[6 + (self as usize * 2)]
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum SlotDescriptor {
    Town(TownSide),
    Road(RoadIndex),
    Robber,
}


#[derive(Debug)]
pub enum SlotRef<'board> {
    Town  (&'board mut Option<Town  >),
    Road  (&'board mut Option<Road  >),
    Robber(&'board mut Option<Robber>),
}

impl<'board> SlotRef<'board> {
    /*pub fn contains_piece(&self) -> bool {
        match self {
            Self::Town  (slot) => slot.is_some(),
            Self::Road  (slot) => slot.is_some(),
            Self::Robber(slot) => slot.is_some(),
        }
    }*/

    pub fn try_fill(&mut self, piece: Piece) -> Result<(), ()> {
        match (self, piece) {
            (Self::Town  (slot @ None), Piece::Town  (piece)) => { **slot = Some(piece); }
            (Self::Road  (slot @ None), Piece::Road  (piece)) => { **slot = Some(piece); }
            (Self::Robber(slot @ None), Piece::Robber(piece)) => { **slot = Some(piece); }
            _ => return Err(()),
        }

        Ok(())
    }

    pub fn remove(&mut self) -> Option<Piece> {
        match self {
            Self::Town  (slot) => slot.take().map(Piece::Town  ),
            Self::Road  (slot) => slot.take().map(Piece::Road  ),
            Self::Robber(slot) => slot.take().map(Piece::Robber),
        }
    }
}


#[derive(Debug)] pub enum SlotData {
    Town  (Option<Town  >),
    Road  (Option<Road  >),
    Robber(Option<Robber>),
}

impl SlotData {
    pub fn into_piece(self) -> Option<Piece> {
        match self {
            SlotData::Town  (Some(town  )) => Some(Piece::Town  (town  )),
            SlotData::Road  (Some(road  )) => Some(Piece::Road  (road  )),
            SlotData::Robber(Some(Robber)) => Some(Piece::Robber(Robber)),
            _ => None,
        }
    }
}

impl From<SlotRef<'_>> for SlotData {
    fn from(slot_ref: SlotRef<'_>) -> Self {
        match slot_ref {
            SlotRef::Town  (town  ) => SlotData::Town  (town  .clone()),
            SlotRef::Road  (road  ) => SlotData::Road  (road  .clone()),
            SlotRef::Robber(robber) => SlotData::Robber(robber.clone()),
        }
    }
}


#[derive(Debug)]
pub struct Tile {
    pub kind: TileKind,
    pub town: (Option<Town>, TownSide), // Separated for selection of empty slots with a predefined side
    pub roads: [Option<Road>; RoadIndex::COUNT], // left, left bottom, right bottom
    pub robber: Option<Robber>,
}

impl Tile {
    pub fn has_dice_value(&self, value: DiceValue) -> bool {
        matches!(self.kind, TileKind::Land(_, number) if number == value)
    }

    pub fn slot_ref(&mut self, descriptor: SlotDescriptor) -> SlotRef {
        match descriptor {
            SlotDescriptor::Town(side) => {
                if !self.town_is_on(side) {
                    debug_assert!(self.empty_town(), "Invalid SlotDescriptor; opposite side town occupied");
                    self.town.1 = side;
                }
                SlotRef::Town(&mut self.town.0)
            }
            SlotDescriptor::Road(index) => SlotRef::Road(&mut self.roads[index as usize]),
            SlotDescriptor::Robber => SlotRef::Robber(&mut self.robber),
        }
    }

    pub fn slot_data(&self, descriptor: SlotDescriptor) -> SlotData {
        match descriptor {
            SlotDescriptor::Town(side) => {
                debug_assert!(
                    self.town_is_on(side) || self.empty_town(),
                    "Invalid SlotDescriptor; observing wrong town side",
                );
                SlotData::Town(self.town.0.clone())
            }
            SlotDescriptor::Road(index) => SlotData::Road(self.roads[index as usize].clone()),
            SlotDescriptor::Robber => SlotData::Robber(self.robber.clone()),
        }
    }

    pub fn roads(&self) -> impl Iterator<Item=(&'static RoadIndex, &'_ Road)> {
        self.roads.iter()
            .zip(RoadIndex::ALL.iter())
            .filter_map(|(road, index)| road.as_ref().map(|road| (index, road)))
    }

    pub fn town(&self) -> Option<(Town, TownSide)> {
        match self.town {
            (Some(ref town), side) => Some((town.clone(), side)),
            (None, _) => None,
        }
    }

    fn empty_town(&self) -> bool {
        self.town.0.is_none()
    }

    fn town_is_on(&self, side: TownSide) -> bool {
        self.town.1 == side
    }
}

impl From<TileKind> for Tile {
    fn from(kind: TileKind) -> Self {
        Self {
            kind,
            town: (None, TownSide::Left),
            roads: [None, None, None],
            robber: if kind.is_desert() { Some(Robber) } else { None },
        }
    }
}
