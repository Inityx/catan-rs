use std::{
    fs,
    io,
    path::{Path, PathBuf},
    process::exit,
};

use shaderc::{Compiler as ShaderCompiler, ShaderKind};

const SHADER_SRC: &str = "assets/src/shaders";
const SHADER_BUILD: &str = "assets/build/shaders";

fn compile_shader(src_path: &Path, compiler: &mut ShaderCompiler) {
    let kind = match src_path.extension().unwrap().to_str() {
        Some("vert") => ShaderKind::Vertex,
        Some("frag") => ShaderKind::Fragment,
        _ => panic!("Unsupported shader type at {}", src_path.to_string_lossy()),
    };

    let artifact = compiler
        .compile_into_spirv(
            &fs::read_to_string(&src_path).unwrap(),
            kind,
            &src_path.to_string_lossy(),
            "build.rs",
            None,
        )
        .unwrap_or_else(|error| {
            eprintln!("{}", error);
            exit(1);
        });

    let out_path = {
        let mut file_name = src_path.file_name().unwrap().to_os_string();
        file_name.push(".spv");

        let mut path = PathBuf::from(SHADER_BUILD);
        path.push(file_name);
        path
    };

    fs::write(out_path, &artifact.as_binary_u8()).unwrap();
}

fn main() {
    println!("cargo:rerun-if-changed={}/*", SHADER_SRC);

    fs::create_dir_all(SHADER_BUILD).unwrap();

    let mut compiler = ShaderCompiler::new().unwrap();

    fs::read_dir(SHADER_SRC)
        .unwrap()
        .map(io::Result::unwrap)
        .filter(|entry| entry.file_type().unwrap().is_file())
        .for_each(|entry| compile_shader(&entry.path(), &mut compiler));
}
