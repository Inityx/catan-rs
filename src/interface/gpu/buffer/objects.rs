use crate::{
    interface::colors::Color,
    geom::{Location, Rotation, Scalar},
};
use cgmath::Vector2;

#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Camera {
    pub offset: Location,
    pub scale: Vector2<Scalar>,
}

impl Default for Camera {
    fn default() -> Self {
        Self {
            offset: Location::new(0.0, 0.0),
            scale: Vector2::new(1.0, 1.0),
        }
    }
}

#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, Default)]
pub struct Uniforms {
    pub camera: Camera,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Instance {
    pub color: Color,
    pub location: Location,
    pub rotation: Rotation,
}

unsafe impl bytemuck::Pod for Instance {}
unsafe impl bytemuck::Zeroable for Instance {}

unsafe impl bytemuck::Pod for Uniforms {}
unsafe impl bytemuck::Zeroable for Uniforms {}
