use hex2d::Coordinate;

pub struct Located<T>(pub Coordinate<isize>, pub T);

#[allow(dead_code)]
impl<T> Located<T> {
    pub fn map<U>(self, func: impl FnOnce(T) -> U) -> Located<U> {
        let Located(coord, item) = self;
        Located(coord, func(item))
    }
}

impl<T> From<(&Coordinate<isize>, T)> for Located<T> {
    fn from((&coord, item): (&Coordinate<isize>, T)) -> Self {
        Located(coord, item)
    }
}
