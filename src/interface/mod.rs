use crate::geom::Vector;

pub mod controller;
mod colors;
mod interaction;
mod gpu;
mod scan;

use colors::Color;
use winit::dpi::PhysicalSize;

const RESIZABLE: bool = true;
const INIT_SIZE: PhysicalSize<u32> = PhysicalSize { width: 512, height: 512 };
const BOARD_MARGINS: Vector = Vector::new(0.9, 0.9);
// const FONT_SOURCE: &[u8] = include_bytes!("../../assets/src/FiraSans-Regular.ttf");
const BACKGROUND_COLOR: wgpu::Color = Color::SEA.to_wgpu();

// pub fn run() {
//     // Choose play game/show board
//     unimplemented!();
// }
