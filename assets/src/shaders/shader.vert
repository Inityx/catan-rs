#version 450
#extension GL_ARB_separate_shader_objects : enable

// structures
struct Camera {
    vec2 offset;
    vec2 scale;
};

// IO
layout(location = 0) in vec2 vertPosition;
layout(location = 1) in vec4 instanceColor;
layout(location = 2) in vec2 instancePosition;
layout(location = 3) in float instanceRotation;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0)
uniform Uniforms {
    Camera camera;
};

// definitions
const float W = 1.0;
const float Z = 0.0;

mat2 rotate(float theta) {
    float s = sin(theta), c = cos(theta);
    return mat2(c, -s, s, c);
}

vec2 camera_transform(vec2 vert) {
    return (vert + camera.offset) * camera.scale;
}

void main() {
    outColor = instanceColor;
    vec2 instance = (rotate(instanceRotation) * vertPosition) + instancePosition;
    gl_Position = vec4(camera_transform(instance), Z, W);
}
