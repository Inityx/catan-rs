use hex2d::Coordinate;
use crate::geom::CoordinateExt;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Bounds {
    pub min: Coordinate<isize>,
    pub max: Coordinate<isize>,
}

impl Bounds {
    pub const INSIDE_OUT: Self = Self {
        min: Coordinate { x: isize::MAX, y: isize::MAX },
        max: Coordinate { x: isize::MIN, y: isize::MIN },
    };

    pub fn expand(self, coord: Coordinate<isize>) -> Self {
        use std::cmp::{min, max};
        Self {
            min: Coordinate::new(min(self.min.x, coord.x), min(self.min.y, coord.y)),
            max: Coordinate::new(max(self.max.x, coord.x), max(self.max.y, coord.y)),
        }
    }

    pub fn span(self) -> Coordinate<isize> {
        Coordinate::new(
            self.max.x - self.min.x,
            self.max.y - self.min.y,
        )
    }
}

impl Default for Bounds {
    fn default() -> Self {
        Self { min: Coordinate::origin(), max: Coordinate::origin() }
    }
}

#[cfg(test)]
mod tests {
    use std::array;
    use super::*;

    #[test]
    fn origin() {
        assert_eq!(
            Bounds::default(),
            Bounds::INSIDE_OUT.expand(Coordinate::origin()),
        );
    }

    #[test]
    fn multiple() {
        assert_eq!(
            Bounds {
                min: Coordinate::new(-3, 1),
                max: Coordinate::new(2, 5),
            },
            array::IntoIter::new([(2, 1), (-3, 4), (1, 5)])
                .map(|(x, y)| Coordinate { x, y })
                .fold(Bounds::INSIDE_OUT, Bounds::expand)
        );
    }

    #[test]
    fn span() {
        assert_eq!(
            Coordinate::new(3, 4),
            Bounds {
                min: Coordinate::new(2, 3),
                max: Coordinate::new(5, 7),
            }.span(),
        );
        assert_eq!(
            Coordinate::new(4, 7),
            Bounds {
                min: Coordinate::new(-1, 0),
                max: Coordinate::new(3, 7),
            }.span(),
        );
    }
}
