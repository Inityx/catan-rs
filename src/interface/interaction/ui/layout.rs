use std::{
    iter::{Zip, Enumerate},
    slice::Iter as SliceIter,
};
use cgmath::{ElementWise, Vector2, EuclideanSpace};

use crate::geom::{Scalar, Location, BoundingBox, Vector, Geom2Ext};
use super::SubWidget;
use itertools::Itertools;

#[allow(dead_code)]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Position {
    LeftTop, LeftMid, LeftBottom,
    MidTop, MidMid, MidBottom,
    RightTop, RightMid, RightBottom,
}

impl Position {
    pub fn relative_coords(self) -> Vector {
        use Position::*;
        match self {
            LeftTop     => (0.0, 0.0),
            LeftMid     => (0.0, 0.5),
            LeftBottom  => (0.0, 1.0),
            MidTop      => (0.5, 0.0),
            MidMid      => (0.5, 0.5),
            MidBottom   => (0.5, 1.0),
            RightTop    => (1.0, 0.0),
            RightMid    => (1.0, 0.5),
            RightBottom => (1.0, 1.0),
        }.into()
    }

    pub fn child_offset(self, child_proportion: Vector2<Scalar>) -> Location {
        let offset = self.relative_coords();
        Location::from_vec(
            offset - offset.mul_element_wise(child_proportion)
        )
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Orientation { Horizontal, Vertical }

impl Orientation {
    fn flip_verticals<V: Geom2Ext>(&self, value: V) -> V { match self {
        Self::Horizontal => value,
        Self::Vertical => value.flip(),
    }}
}

#[allow(dead_code)]
#[derive(Debug)]
pub enum Children<Message> {
    None,
    Anchored(Box<[Position]>, Box<[SubWidget<Message>]>),
    Aligned(Orientation, Box<[SubWidget<Message>]>),
}

#[allow(dead_code)]
impl<Message> Children<Message> {
    pub fn anchored(widgets: impl IntoIterator<Item=(Position, SubWidget<Message>)>) -> Self {
        let (positions, children): (Vec<_>, Vec<_>) = widgets.into_iter().unzip();
        Self::Anchored(positions.into_boxed_slice(), children.into_boxed_slice())
    }

    pub(super) fn validated(self) -> Self {
        match &self {
            Self::None => {}
            Self::Anchored(positions, widgets) => {
                assert_eq!(positions.len(), widgets.len());
                assert!(!widgets.is_empty(), "Requested empty Children::Anchored");
            }
            Self::Aligned(_, widgets) => {
                assert!(!widgets.is_empty(), "Requested empty Children::Aligned");
            }
        }

        for (_, child_bounding_box) in self.bounding_boxes() {
            assert!(BoundingBox::NORMAL.encompasses(child_bounding_box, super::TOLERANCE), "Leaking child bounding box {:?}", child_bounding_box);
        }

        for (a, b) in self.bounding_boxes().map(|(_, bbox)| bbox).tuple_combinations() {
            assert!(!a.overlaps(b, super::TOLERANCE), "Overlapping child bounding boxes:\n\t{:?}\n\t{:?}", a, b);
        }

        self
    }

    pub fn len(&self) -> usize { match self {
        Self::None => 0,
        Self::Anchored(_, children) |
        Self::Aligned(_, children) => children.len(),
    }}

    fn bounding_boxes(&self) -> ChildBounds<'_, Message> {
        match self {
            Self::None => ChildBounds::None,
            Self::Anchored(positions, children) => ChildBounds::Anchored {
                children: positions.iter().zip(children.as_ref()),
            },
            Self::Aligned(orientation, children) => ChildBounds::Aligned {
                orientation: *orientation,
                child_count: children.len(),
                children: children.iter().enumerate(),
            },
        }
    }

    pub fn probe_at(&self, location: Location) -> Option<(Location, &SubWidget<Message>)> {
        self.bounding_boxes()
            .find(|(_, bbox)| bbox.contains(location, super::TOLERANCE))
            .map(|(child, bbox)| (bbox.relative(location), child))
    }
}

enum ChildBounds<'a, Message> {
    None,
    Anchored {
        children: Zip<SliceIter<'a, Position>, SliceIter<'a, SubWidget<Message>>>,
    },
    Aligned {
        orientation: Orientation,
        child_count: usize,
        children: Enumerate<SliceIter<'a, SubWidget<Message>>>,
    },
}

// Cannot be derived because `derive` blindly looks at type parameters
impl<Message> Clone for ChildBounds<'_, Message> {
    fn clone(&self) -> Self { match self {
        &ChildBounds::None => ChildBounds::None,
        &ChildBounds::Anchored { ref children } => ChildBounds::Anchored {
            children: children.clone(),
        },
        &ChildBounds::Aligned { orientation, child_count, ref children } => ChildBounds::Aligned {
            orientation,
            child_count,
            children: children.clone(),
        },
    }}
}

impl<'a, Message> Iterator for ChildBounds<'a, Message> {
    type Item = (&'a SubWidget<Message>, BoundingBox);

    fn next(&mut self) -> Option<Self::Item> { match self {
        &mut Self::None => None,
        &mut Self::Anchored { ref mut children } => {
            let (position, child) = children.next()?;

            let child_offset = position.child_offset(child.size);
            Some((child, BoundingBox::offset_size(child_offset, child.size)))
        }
        &mut Self::Aligned {
            orientation,
            child_count,
            ref mut children,
        } => {
            let segment_width = 1.0 / child_count as Scalar;
            let (i, child) = children.next()?;

            let child_offset = Location::new(segment_width * (i as Scalar + 0.5), 0.5) -
                orientation.flip_verticals(child.size) / 2.0;

            let child_offset = orientation.flip_verticals(child_offset);
            Some((child, BoundingBox::offset_size(child_offset, child.size)))
        }
    }}
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::super::{Widget, SubWidget};

    #[test]
    fn single() {
        const SCREEN: Vector = Vector::new(1.0, 1.0);
        const SIZE: Vector = Vector::new(0.2, 0.3);

        const INSIDE: Location = Location::new(0.5, 0.5);
        const OUTSIDE: Location = Location::new(0.5, 0.1);

        let w = Widget::new(SCREEN, Position::MidMid, Children::Anchored(
            Box::new([Position::MidMid]),
            Box::new([SubWidget::new(SIZE, Some(1), Children::None)]),
        ));

        let mut events = w.ray_intersect(SCREEN, INSIDE);
        assert_eq!(Some(&1), events.next());
        assert_eq!(None, events.next());

        let mut events = w.ray_intersect(SCREEN, OUTSIDE);
        assert_eq!(None, events.next());
    }

    #[test]
    fn nested_single_aligned() {
        const OUTER: Vector = Vector::new(1.0, 1.0);
        const MIDDLE: Vector = Vector::new(0.5, 0.5);
        const INNER: Vector = Vector::new(0.5, 0.5);

        const CENTER: Location = Location::new(0.5, 0.5);
        const OFF_CENTER: Location = Location::new(0.251, 0.251);

        let w = Widget::new(OUTER, Position::MidMid, Children::Aligned(Orientation::Horizontal, Box::new([
            SubWidget::new(MIDDLE, Some(1), Children::Aligned(Orientation::Horizontal, Box::new([
                SubWidget::new(INNER, Some(2), Children::None),
            ]))),
        ])));

        let mut events = w.ray_intersect(OUTER, CENTER);
        assert_eq!(Some(&1), events.next());
        assert_eq!(Some(&2), events.next());
        assert_eq!(None, events.next());

        let mut events = w.ray_intersect(OUTER, OFF_CENTER);
        assert_eq!(Some(&1), events.next());
        assert_eq!(None, events.next());
    }

    #[test]
    fn nested_tree_aligned() {
        const SCREEN: Vector = Vector::new(3.0, 2.0);
        const OUTER: Vector = Vector::new(1.0, 1.0);
        const SIZE: Vector = Vector::new(0.5, 1.0);

        const FIRST: Location = Location::new(1.0, 1.0);
        const SECOND: Location = Location::new(2.0, 1.0);

        let w = Widget::new(OUTER, Position::MidMid, Children::Aligned(Orientation::Horizontal, Box::new([
            SubWidget::new(SIZE, Some(1), Children::None),
            SubWidget::new(SIZE, Some(2), Children::Aligned(Orientation::Horizontal, Box::new([
                SubWidget::new(SIZE, None, Children::None),
            ]))),
        ])));

        let mut events = w.ray_intersect(SCREEN, FIRST);
        assert_eq!(Some(&1), events.next());
        assert_eq!(None, events.next());

        let mut events = w.ray_intersect(SCREEN, SECOND);
        assert_eq!(Some(&2), events.next());
        assert_eq!(None, events.next());
    }

    #[test]
    fn perfect_fill() {
        const OUTER: Vector = Vector::new(1.0, 0.5);
        const QUARTER: Vector = Vector::new(0.25, 1.0);

        let w = Widget::<()>::new(OUTER, Position::MidMid, Children::Aligned(Orientation::Horizontal, Box::new([
            SubWidget::new(QUARTER, None, Children::None),
            SubWidget::new(QUARTER, None, Children::None),
            SubWidget::new(QUARTER, None, Children::None),
            SubWidget::new(QUARTER, None, Children::None),
        ])));

        let bounds: Vec<_> = w.children.bounding_boxes().map(|(_, b)| b).collect();
        assert_eq!(
            vec![
                BoundingBox::offset_size(Location::new(0.00, 0.0), Vector::new(0.25, 1.0)),
                BoundingBox::offset_size(Location::new(0.25, 0.0), Vector::new(0.25, 1.0)),
                BoundingBox::offset_size(Location::new(0.50, 0.0), Vector::new(0.25, 1.0)),
                BoundingBox::offset_size(Location::new(0.75, 0.0), Vector::new(0.25, 1.0)),
            ],
            bounds,
        );
    }

    #[test]
    #[should_panic(expected = "Intersecting child bounding boxes")]
    fn intersecting_children() {
        Widget::<()>::new((1.0, 1.0).into(), Position::MidMid, Children::anchored(vec![
            (Position::MidTop, SubWidget::new((0.4, 0.4).into(), None, Children::None)),
            (Position::MidMid, SubWidget::new((0.3, 0.3).into(), None, Children::None)),
        ]));
    }
}
