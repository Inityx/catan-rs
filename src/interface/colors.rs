#![allow(dead_code)]
use crate::{geom::Scalar, board::{tile::TileKind, defs::{PlayerColor, Resource}}};

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Color(
    pub Scalar,
    pub Scalar,
    pub Scalar,
    pub Scalar,
);

macro_rules! color_defs {
    ($([$r:expr, $g:expr, $b:expr, $a:expr] $name:ident)+) => {
        $(pub const $name: Color = Color($r, $g, $b, $a);)+
    };
}

impl Color {
    color_defs! {
        [ 0.000, 0.000, 0.000, 0.000 ] TRANSPARENT
        [ 0.000, 0.000, 0.000, 1.000 ] BLACK
        [ 1.000, 1.000, 1.000, 1.000 ] WHITE
        [ 1.000, 1.000, 1.000, 0.500 ] HIGHLIGHT

        [ 1.000, 1.000, 0.878, 1.000 ] CHIP
        [ 0.000, 0.000, 0.000, 0.500 ] ROBBER
        [ 0.294, 0.569, 0.824, 1.000 ] HARBOR
        [ 0.349, 0.780, 1.000, 1.000 ] SEA
        [ 0.824, 0.706, 0.549, 1.000 ] DESERT
        [ 0.698, 0.133, 0.100, 1.000 ] BRICK
        [ 0.545, 0.271, 0.075, 1.000 ] LUMBER
        [ 0.600, 0.600, 0.681, 1.000 ] ORE
        [ 1.000, 0.843, 0.000, 1.000 ] GRAIN
        [ 0.133, 0.545, 0.133, 1.000 ] WOOL

        [ 0.000, 0.000, 1.000, 1.000 ] PLAYER_BLUE
        [ 1.000, 0.000, 0.000, 1.000 ] PLAYER_RED
        [ 0.000, 1.000, 0.000, 1.000 ] PLAYER_GREEN
        [ 1.000, 1.000, 1.000, 1.000 ] PLAYER_WHITE
        [ 1.000, 0.200, 0.000, 1.000 ] PLAYER_ORANGE
        [ 0.300, 0.060, 0.000, 1.000 ] PLAYER_BROWN
    }

    pub const fn to_wgpu(self) -> wgpu::Color {
        let Color(r, g, b, a) = self;
        wgpu::Color {
            r: r as f64,
            g: g as f64,
            b: b as f64,
            a: a as f64,
        }
    }
}

impl From<Resource> for Color {
    fn from(resource: Resource) -> Self {
        use Resource::*;
        match resource {
            Brick  => Self::BRICK,
            Lumber => Self::LUMBER,
            Ore    => Self::ORE,
            Grain  => Self::GRAIN,
            Wool   => Self::WOOL,
        }
    }
}

impl From<&Resource> for Color {
    fn from(resource: &Resource) -> Self { Self::from(*resource) }
}

impl From<TileKind> for Color {
    fn from(kind: TileKind) -> Self {
        match kind {
            TileKind::Land(resource, _) => Self::from(resource),
            TileKind::Water(_) => Self::SEA,
            TileKind::Desert => Self::DESERT,
        }
    }
}

impl From<PlayerColor> for Color {
    fn from(color: PlayerColor) -> Self {
        use PlayerColor::*;
        match color {
            Blue   => Color::PLAYER_BLUE,
            Red    => Color::PLAYER_RED,
            Green  => Color::PLAYER_GREEN,
            White  => Color::PLAYER_WHITE,
            Orange => Color::PLAYER_ORANGE,
            Brown  => Color::PLAYER_BROWN,
        }
    }
}

impl From<&PlayerColor> for Color {
    fn from(board_color: &PlayerColor) -> Self { Self::from(*board_color) }
}
