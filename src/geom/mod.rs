use std::fmt;
use hex2d::{Coordinate, Integer, Spacing};
use cgmath::{Vector2, Point2, Rad, EuclideanSpace, ElementWise, relative_eq};

macro_rules! geom_pairs {
    (let scale = $scale:expr; $t:ty, $([$x:expr, $y:expr],)*) => {
        [$(<$t>::new($scale * $x, $scale * $y),)*]
    };
    ($t:ty, $([$x:expr, $y:expr],)*) => {
        geom_pairs!{
            let scale = 1.0;
            $t, $([$x, $y],)*
        }
    };
}

pub mod shapes;
pub mod spiral;
pub mod bounds;
pub mod hour;
pub mod located;
pub mod mesh;

pub type Scalar = f32;
pub type Location = Point2<Scalar>;
pub type Vector = Vector2<Scalar>;
pub type Rotation = Rad<Scalar>;
pub type VertIndex = u16;

#[derive(Clone, Copy, PartialEq)]
pub struct BoundingBox {
    lesser: Location,
    greater: Location,
}

impl BoundingBox {
    pub const NORMAL: Self = Self {
        lesser: Location::new(0.0, 0.0),
        greater: Location::new(1.0, 1.0),
    };

    pub fn offset_size(offset: Location, size: Vector) -> Self {
        Self {
            lesser: offset,
            greater: offset + size
        }.validated()
    }

    fn validated(self) -> Self {
        assert!(
            self.lesser.x < self.greater.x &&
            self.lesser.y < self.greater.y,
            "Invalid bounding box {:?}",
            self
        );
        self
    }

    pub fn relative(&self, location: Location) -> Location {
        (location - self.lesser.to_vec()).div_element_wise(Location::from_vec(self.greater - self.lesser))
    }

    pub fn contains(&self, location: Location, tolerance: f32) -> bool {
        let x_lo = location.x > self.lesser .x || relative_eq!(location.x, self.lesser .x, max_relative = tolerance);
        let y_lo = location.y > self.lesser .y || relative_eq!(location.y, self.lesser .y, max_relative = tolerance);
        let x_hi = location.x < self.greater.x || relative_eq!(location.x, self.greater.x, max_relative = tolerance);
        let y_hi = location.y < self.greater.y || relative_eq!(location.y, self.greater.y, max_relative = tolerance);

        x_lo && y_lo && x_hi && y_hi
    }

    pub fn encompasses(&self, other: Self, tolerance: f32) -> bool {
        self.contains(other.lesser, tolerance) && self.contains(other.greater, tolerance)
    }

    pub fn overlaps(&self, other: Self, tolerance: f32) -> bool {
        let x_l = self.lesser .x > other.greater.x || relative_eq!(self.lesser .x, other.greater.x, max_relative = tolerance);
        let y_l = self.lesser .y > other.greater.y || relative_eq!(self.lesser .y, other.greater.y, max_relative = tolerance);
        let x_r = self.greater.x < other.lesser .x || relative_eq!(self.greater.x, other.lesser .x, max_relative = tolerance);
        let y_r = self.greater.y < other.lesser .y || relative_eq!(self.greater.y, other.lesser .y, max_relative = tolerance);

        !(x_l || y_l || x_r || y_r)
    }
}

impl fmt::Debug for BoundingBox {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "BoundingBox[{}, {}]", DisplayPair(self.lesser.into()), DisplayPair(self.greater.into()))
    }
}

pub struct DisplayPair<S: fmt::Display>(pub (S, S));

impl<S: fmt::Display> fmt::Display for DisplayPair<S> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.0.0, self.0.1)
    }
}

pub trait Geom2Ext {
    type Component;
    fn flip(self) -> Self;
    fn is_normalized(self) -> bool;
    fn greater(self) -> Self::Component;
    fn lesser(self) -> Self::Component;
}

impl Geom2Ext for Vector {
    type Component = Scalar;
    fn flip(self) -> Self {
        let Self { x, y } = self;
        Self { x: y, y: x }
    }

    fn is_normalized(self) -> bool {
        self.x >= 0.0 && self.x <= 1.0 &&
        self.y >= 0.0 && self.y <= 1.0
    }

    fn greater(self) -> Self::Component {
        let Self { x, y } = self;
        scalar_max(x, y)
    }

    fn lesser(self) -> Self::Component {
        let Self { x, y } = self;
        scalar_min(x, y)
    }
}

impl Geom2Ext for Location {
    type Component = Scalar;
    fn flip(self) -> Self {
        let Self { x, y } = self;
        Self { x: y, y: x }
    }

    fn is_normalized(self) -> bool {
        self.x >= 0.0 && self.x <= 1.0 &&
        self.y >= 0.0 && self.y <= 1.0
    }

    fn greater(self) -> Self::Component {
        let Self { x, y } = self;
        scalar_max(x, y)
    }

    fn lesser(self) -> Self::Component {
        let Self { x, y } = self;
        scalar_min(x, y)
    }
}

pub trait CoordinateExt {
    type Component;
    fn origin() -> Self;
    fn is_ring_corner(&self) -> bool;
    fn is_ring_start(&self) -> bool;
    fn is_zero(&self) -> bool;
    fn to_cartesian(&self, spacing: fn(f32) -> Spacing) -> Location;
    fn xy_as_vector2(&self) -> Vector2<Self::Component>;
}

impl<I: Integer> CoordinateExt for Coordinate<I> {
    type Component = I;

    #[inline(always)]
    fn origin() -> Self { Self::new(I::zero(), I::zero()) }

    #[inline(always)]
    fn is_ring_corner(&self) -> bool {
        self.x.is_zero() || self.y.is_zero() || self.z().is_zero()
    }

    #[inline(always)]
    fn is_ring_start(&self) -> bool {
        self.x.is_zero() && self.y.is_positive()
    }

    #[inline(always)]
    fn is_zero(&self) -> bool {
        self.x.is_zero() && self.y.is_zero()
    }

    #[inline(always)]
    fn to_cartesian(&self, spacing: fn(f32) -> Spacing) -> Location {
        let (x, y) = self.to_pixel(spacing(1.0));
        Location { x, y: -y } // the library makes cartesian Y inverted
    }

    #[inline(always)]
    fn xy_as_vector2(&self) -> Vector2<Self::Component> {
        let Coordinate { x, y } = *self;
        Vector2 { x, y }
    }
}

fn scalar_min(lhs: Scalar, rhs: Scalar) -> Scalar {
    if lhs < rhs { lhs } else { rhs }
}

fn scalar_max(lhs: Scalar, rhs: Scalar) -> Scalar {
    if lhs < rhs { lhs } else { rhs }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn to_cartesian() {
        assert_eq!(
            Location::new(0.0, 3.0),
            Coordinate::new(1, 1).to_cartesian(Spacing::PointyTop),
        );
    }
}
