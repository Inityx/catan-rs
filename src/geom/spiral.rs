use super::CoordinateExt;
use hex2d::{Coordinate, Direction, Angle, ToCoordinate};

pub struct InwardSpiral(Option<(Coordinate<isize>, Direction)>);

impl InwardSpiral {
    pub fn from_ring(ring: usize) -> Self {
        Self(Some((
            Coordinate::new(0, ring as isize),
            Direction::YX,
        )))
    }
}

impl Iterator for InwardSpiral {
    type Item = Coordinate<isize>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some((mut coord, mut dir)) = self.0 {
            let next = coord;

            self.0 = if coord.is_zero() {
                None // Termination at center
            } else {
                if coord.is_ring_corner() { dir = dir + Angle::Left; }

                coord = coord + dir.to_coordinate();

                // If completed ring, go inward
                if coord.is_ring_start() { coord.y -= 1; }

                Some((coord, dir))
            };

            Some(next)
        } else {
            None
        }
    }
}

impl std::iter::FusedIterator for InwardSpiral {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn spiral_from_ring() {
        let collected: Vec<Coordinate<isize>> = InwardSpiral::from_ring(0).take(2).collect();
        assert_eq!(&[Coordinate::origin()], collected.as_slice());

        let collected: Vec<Coordinate<isize>> = InwardSpiral::from_ring(1).take(8).collect();
        assert_eq!(
            &[
                Coordinate::new( 0,  1),
                Coordinate::new(-1,  1),
                Coordinate::new(-1,  0),
                Coordinate::new( 0, -1),
                Coordinate::new( 1, -1),
                Coordinate::new( 1,  0),

                Coordinate::origin(),
            ],
            collected.as_slice(),
        );

        let collected: Vec<Coordinate<isize>> = InwardSpiral::from_ring(2).take(13).collect();
        assert_eq!(
            &[
                Coordinate::new( 0,  2),
                Coordinate::new(-1,  2),
                Coordinate::new(-2,  2),
                Coordinate::new(-2,  1),
                Coordinate::new(-2,  0),
                Coordinate::new(-1, -1),
                Coordinate::new( 0, -2),
                Coordinate::new( 1, -2),
                Coordinate::new( 2, -2),
                Coordinate::new( 2, -1),
                Coordinate::new( 2,  0),
                Coordinate::new( 1,  1),

                Coordinate::new( 0,  1),
            ],
            collected.as_slice(),
        );
    }
}
