#![allow(dead_code)]
use crate::board::{
    Board,
    piece::{Card, Achievement},
    defs::PlayerColor,
};

pub struct UiState {
    players: Box<[Player]>,
}

pub struct GamePlayInterface {
    ui_state: UiState,
    board: Board,
}

impl GamePlayInterface {
    /// Creates a new game with a custom board.
    ///
    /// Panics if `players` is not between 1 and 6 inclusive.
    pub fn new(player_count: usize, board: Board) -> Self {
        assert!(player_count > 0 && player_count <= 6);

        Self {
            ui_state: UiState {
                players: PlayerColor::ALL.iter().cloned()
                    .map(Player::new)
                    .take(player_count)
                    .collect(),
            },
            board,
        }
    }

    /// Game with a standard (beginner) layout.
    ///
    /// Panics if `players` is not between 1 and 6 inclusive.
    pub fn standard(player_count: usize) -> Self {
        GamePlayInterface::new(player_count, Board::standard_layout())
    }
}

#[derive(Debug)]
pub struct Player {
    color: PlayerColor,
    hand: Vec<Card>,
    achievements: Vec<Achievement>,
}

impl Player {
    pub fn new(color: PlayerColor) -> Self {
        Self {
            color,
            hand: Vec::new(),
            achievements: Vec::new(),
        }
    }

    fn victory_card_count(&self) -> usize {
        self.hand.iter().filter(|card| card.is_victory_point()).count()
    }
}
