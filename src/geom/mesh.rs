#![allow(dead_code)]
use crate::geom::{Scalar, Location, VertIndex, shapes::{ROAD_WIDTH, ROAD_MITER, TOWN_SCALE}};
use std::f32::consts::{PI, FRAC_PI_2};
use cgmath::{Rad, ElementWise};
use super::Rotation;

#[derive(Debug, Clone, Copy, Default)]
pub struct Mesh<V: AsRef<[Location]>, I: AsRef<[VertIndex]>> {
    pub vertices: V,
    pub indices: I,
}

pub type OwnedMesh = Mesh<Vec<Location>, Vec<VertIndex>>;
pub type RefMesh<'a> = Mesh<&'a [Location], &'a [VertIndex]>;

impl<V: AsRef<[Location]>, I: AsRef<[VertIndex]>> Mesh<V, I> {
    pub fn as_ref_mesh(&self) -> RefMesh {
        Mesh {
            vertices: self.vertices.as_ref(),
            indices: self.indices.as_ref(),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Circle {
    /// 1/2 the number of vertices
    pub res: VertIndex,
    pub radius: Scalar,
}

impl Circle {
    /// Render circle as a triangle strip
    #[allow(clippy::many_single_char_names)]
    pub fn to_mesh(self) -> OwnedMesh {
        if self.res < 2 { return OwnedMesh::default(); }

        let vertex_count = self.res * 2;
        let vertices = (0..vertex_count).map(|i| self.vertex_location(i)).collect();

        let last = vertex_count - 1;
        let tri_count = vertex_count - 2;

        let indices = (0..tri_count / 2)
            .flat_map(|quad_index| {
                let a = quad_index;
                let b = a + 1;
                let c = last - quad_index;

                let d = b;
                let f = c;
                let e = f - 1;

                std::array::IntoIter::new([a, b, c, d, e, f])
            })
            .collect();

        Mesh { vertices, indices }
    }

    fn arc(&self) -> Rotation {
        Rad(PI / self.res as Scalar)
    }

    fn vertex_location(&self, i: u16) -> Location {
        let angle = self.arc().0 * i as Scalar + FRAC_PI_2;
        let loc = Location::new(angle.cos(), angle.sin());
        loc.mul_element_wise(self.radius)
    }
}

macro_rules! rect {
    ($name:ident, $size:expr) => {
        rect!($name, $size, $size);
    };
    ($name:ident, $width:expr, $height:expr) => {
        pub const $name: RefMesh = Mesh {
            vertices: &geom_pairs![
                Location,
                [-$width / 2.0, -$height / 2.0],
                [ $width / 2.0, -$height / 2.0],
                [-$width / 2.0,  $height / 2.0],
                [ $width / 2.0,  $height / 2.0],
            ],
            indices: &[
                0,2,1,
                2,3,1,
            ],
        };
    };
}

pub const HEXAGON: Circle = Circle { res: 3, radius: 1.0 };
pub const HOVER_CIRCLE: Circle = Circle { res: 16, radius: 1.0 / 3.0 };

pub const ROAD: RefMesh = Mesh {
    vertices: &geom_pairs![
        Location,
        [ 0.0,        -0.5       ],
        [-ROAD_WIDTH, -ROAD_MITER],
        [ ROAD_WIDTH, -ROAD_MITER],
        [-ROAD_WIDTH,  ROAD_MITER],
        [ ROAD_WIDTH,  ROAD_MITER],
        [ 0.0,         0.5       ],
    ],
    indices: &[
        0,1,2,
        1,3,2,
        2,3,4,
        3,5,4,
    ],
};

pub const CITY: RefMesh = Mesh {
    vertices: &geom_pairs![
        let scale = TOWN_SCALE;
        Location,
        [-1.0, -1.0],
        [ 1.0, -1.0],
        [-1.0,  0.0],
        [ 0.0,  0.0],
        [-0.5,  0.5],
        [ 0.0,  1.0],
        [ 1.0,  1.0],
        [ 0.5,  1.5],
    ],
    indices: &[
        0, 6, 1,
        0, 2, 3,
        2, 4, 3,
        3, 5, 6,
        5, 7, 6,
    ],
};

pub const SETTLEMENT: RefMesh = Mesh {
    vertices: &geom_pairs![
        let scale = TOWN_SCALE;
        Location,
        [-0.5, -0.5],
        [ 0.5, -0.5],
        [-0.5,  0.5],
        [ 0.5,  0.5],
        [ 0.0,  1.0],
    ],
    indices: &[
        0, 3, 1,
        0, 2, 3,
        2, 4, 3,
    ],
};

rect!(CHIP,           0.60);
rect!(ROBBER,         0.50);
rect!(HARBOR,         0.40);
rect!(HARBOR_INSIDE,  0.24);

// pub const HARBOR_BORDER: Radius = 0.03;

#[cfg(test)]
mod tests {
    use super::*;
    use cgmath::{assert_abs_diff_eq, EuclideanSpace, InnerSpace};

    #[test]
    fn circle_empty() {
        let circle = Circle { res: 0, radius: 1.0 }.to_mesh();
        assert_eq!(0, circle.vertices.len());
        assert_eq!(0, circle.indices .len());
    }

    #[test]
    fn circle_line() {
        let circle = Circle { res: 1, radius: 1.0 }.to_mesh();
        assert_eq!(0, circle.vertices.len());
        assert_eq!(0, circle.indices .len());
    }

    #[test]
    fn circle_quad() {
        let circle = Circle { res: 2, radius: 1.0 }.to_mesh();
        assert_eq!(4, circle.vertices.len());
        assert_eq!(6, circle.indices .len());
    }

    #[test]
    fn circle_nominal() {
        let circle = Circle { res: 16, radius: 1.0 }.to_mesh();
        assert_eq!(32, circle.vertices.len());
        assert_eq!(90, circle.indices .len());
        for vertex in &circle.vertices {
            assert_abs_diff_eq!(1.0, vertex.to_vec().magnitude());
        }
    }

    #[test]
    fn circle_radius() {
        let circle = Circle { res: 4, radius: 0.25 }.to_mesh();
        assert_eq!(8,  circle.vertices.len());
        assert_eq!(18, circle.indices .len());
        for vertex in &circle.vertices {
            assert_abs_diff_eq!(0.25, vertex.to_vec().magnitude());
        }
    }
}
