use crate::geom::Scalar;
use cgmath::{Rad, Vector2};
use hex2d::Spacing;
use super::Rotation;

pub const STRAIGHT: Rotation = Rad(0.0);

pub const RADIUS_TO_DIAMETER: Scalar = 2.0;
pub const SQRT_3: Scalar = 1.732_050_8;

pub mod hex {
    use super::*;

    pub const ORIGIN: Scalar = 0.0;
    pub const MAJOR: Scalar = 1.0;
    pub const MINOR: Scalar = SQRT_3 / 2.0;
    pub const DIAG_Y: Scalar = MINOR / 2.0 * SQRT_3;
    pub const GRID_ASPECT: Vector2<Scalar> = Vector2 { x: MINOR, y: MINOR * MINOR };
}

pub const TILE_SPACING: fn(f32) -> Spacing = Spacing::PointyTop;

pub const ROAD_WIDTH: Scalar = 0.075;
pub const ROAD_MITER: Scalar = 0.5 - (ROAD_WIDTH / SQRT_3);
pub const TOWN_SCALE: Scalar = 1.0 / 3.0;
