#![allow(dead_code)]

use crate::{geom::{Scalar, Vector}, board::{
    defs::PlayerColor,
    Board,
    piece::TownKind, self,
}};
use crate::interface::{
    interaction::{
        input::InputLayer,
        ui::{Widget, SubWidget, layout},
        view::ViewLayer,
        dirtmap::Dirtmapped,
    },
    gpu::{frame, Gpu, buffer::{
        proxy::{Models, UniformBufferProxy},
        objects::Instance,
    }},
    RESIZABLE,
    INIT_SIZE,
    colors::Color,
    scan::Instancer,
};
use winit::{event_loop::EventLoop, window::{WindowBuilder, Window}, dpi::PhysicalSize};
use board::Selection;

const TITLE: &str = "Catan Sandbox";

mod brush {
    use super::*;
    use crate::geom::{Geom2Ext, Location};

    #[derive(Debug, Clone)]
    pub enum Kind {
        Town(TownKind),
        Road,
        Robber,
    }

    impl Kind {
        pub const ALL: [Self; 4] = [
            Self::Town(TownKind::Settlement),
            Self::Town(TownKind::City),
            Self::Road,
            Self::Robber,
        ];
    }

    #[derive(Debug, Clone)]
    pub enum Change {
        Color(PlayerColor),
        Kind(Kind),
    }

    pub struct Brush {
        pub color: Option<PlayerColor>,
        pub kind: Option<Kind>,
        pub widget: Widget<Change>,
    }

    impl Brush {
        pub fn new(widget: Widget<Change>) -> Self {
            Self { color: None, kind: None, widget }
        }

        pub fn click(&mut self, screen_size: Vector, location: Location) {
            let major = screen_size.greater();
            for change in self.widget.ray_intersect(screen_size / major, location / major) {
                match change {
                    Change::Color(color) => self.color = Some(color.clone()),
                    Change::Kind(kind) => self.kind = Some(kind.clone()),
                }
            }
        }
    }
}

pub struct Controller {
    gpu: Gpu,
    models: Models,
    uniforms: UniformBufferProxy,
    board: Board,
    window: Window,
    input_layer: InputLayer,
    view_layer: Dirtmapped<ViewLayer>,
    selection: Dirtmapped<Option<Selection>>,
    brush: brush::Brush,
}

impl Controller {
    pub fn play(board: Board) -> ! {
        let event_loop = EventLoop::new();
        let mut interface = Self::new(board, &event_loop);

        event_loop.run(move |event, _, control_flow| {
            *control_flow = interface.handle_event(event);
        });
    }

    fn new(board: Board, event_loop: &EventLoop<()>) -> Self {
        let window = WindowBuilder::new()
            .with_title(TITLE)
            .with_inner_size(INIT_SIZE)
            .with_resizable(RESIZABLE)
            .build(&event_loop)
            .expect("Game window creation failed");

        let gpu = Gpu::new(&window).expect("GPU initialization failed");

        let models = {
            let instances_of = Instancer(&board);
            Models::new(
                &gpu.device,
                &instances_of.tiles(),
                &instances_of.roads(),
                &instances_of.towns(TownKind::Settlement),
                &instances_of.towns(TownKind::City),
                &instances_of.robbers(),
            )
        };

        let brush = brush::Brush::new(Widget::new(
            (0.5, 0.2).into(),
            layout::Position::MidBottom,
            layout::Children::Aligned(layout::Orientation::Vertical, Box::new([
                SubWidget::new((1.0, 0.5).into(), None, layout::Children::Aligned(
                    layout::Orientation::Horizontal,
                    PlayerColor::ALL.iter().copied().map(brush::Change::Color).map(|change| SubWidget::new(
                        (1.0 / PlayerColor::ALL.len() as Scalar, 1.0).into(),
                        Some(change),
                        layout::Children::None,
                    )).collect()
                )),
                SubWidget::new((1.0, 0.5).into(), None, layout::Children::Aligned(
                    layout::Orientation::Horizontal,
                    brush::Kind::ALL.iter().cloned()
                        .map(brush::Change::Kind)
                        .map(|change| SubWidget::new(
                            (1.0 / PlayerColor::ALL.len() as Scalar, 1.0).into(),
                            Some(change),
                            layout::Children::None,
                        ))
                        .collect()
                    ),
                ),
            ]),
        )));

        Self {
            uniforms: UniformBufferProxy::new(&gpu, Default::default()),
            view_layer: Dirtmapped::clean(ViewLayer::new(&board, INIT_SIZE)),
            models,
            gpu,
            window,
            board,
            input_layer: Default::default(),
            selection: Default::default(),
            brush,
        }
    }

    fn handle_event(&mut self, event: winit::event::Event<()>) -> winit::event_loop::ControlFlow {
        use winit::{
            event::{Event, WindowEvent},
            event_loop::ControlFlow,
        };

        match event {
            Event::WindowEvent { event: WindowEvent::CloseRequested, .. } => return ControlFlow::Exit,
            Event::WindowEvent {
                event: WindowEvent::Resized(PhysicalSize { width, height }),
                ..
            } => self.recompute_size(Some(Vector::new(width as Scalar, height as Scalar))),

            Event::WindowEvent { event, .. } => {
                if self.input_layer.accept(event) {
                    if let Some(cursor) = self.input_layer.cursor {
                        self.selection.write(self.view_layer.get().compute_selection(&self.board, cursor));
                    }

                    // Debug print selection contents
                    // if let Some(selection @ Selection { coord: Coordinate { x, y }, hour }) = self.selection {
                    //     eprintln!("Board[{}, {}, {:?}] = {:?}", x, y, hour, self.board.get(selection));
                    // }

                    // Debug UI
                    if let Some(cursor) = self.input_layer.cursor {
                        eprintln!(
                            "UI hover: {:?}",
                            self.brush.widget.ray_intersect(
                                self.view_layer.get().viewport(),
                                cursor,
                            ).collect::<Vec<_>>(),
                        );
                    }

                    self.window.request_redraw();
                }
            }

            Event::RedrawRequested(window_id) if window_id == self.window.id() => {
                self.refresh_buffers();

                frame::render(
                    &mut self.gpu,
                    &self.uniforms.bind_group,
                    &self.models,
                    *self.selection.get(),
                );
            },
            _ => {}
        }

        ControlFlow::Wait
    }

    fn recompute_size(&mut self, new_viewport: Option<Vector>) {
        // Resize GPU surface
        if let Some(Vector { x, y }) = new_viewport {
            self.gpu.resize(PhysicalSize { width: x as u32, height: y as u32 });
        }

        // Resize state viewport
        let board = &self.board;
        self.view_layer.modify(|view| view.resize_viewport(
            board,
            new_viewport.unwrap_or_else(|| view.viewport())
        ));
    }

    fn refresh_buffers(&mut self) {
        use std::intrinsics::unlikely;

        // Sync UI
        let models = &mut self.models;
        let gpu = &self.gpu;
        let uniforms = &mut self.uniforms;

        self.view_layer.sync(|view| {
            uniforms.value.modify(&gpu.device, |uniforms| {
                uniforms.camera.scale = view.viewport_scale();
            });
        });

        self.selection.sync(|selection| {
            let instance = selection
                .map(|selection| Instance {
                    location: selection.location(),
                    color: Color::HIGHLIGHT,
                    rotation: selection.rotation(),
                });

            models.hover.replace(&gpu.device, instance);
        });

        // Sync board
        if unlikely(self.board.has_modified(board::Dirtmap::TILES)) {
            self.models.tiles.instances.replace(
                &self.gpu.device,
                &Instancer(&self.board).tiles(),
            );

            self.recompute_size(None);
        }

        if unlikely(self.board.has_modified(board::Dirtmap::ROADS)) {
            self.models.roads.instances.replace(
                &self.gpu.device,
                &Instancer(&self.board).roads(),
            );
        }

        if unlikely(self.board.has_modified(board::Dirtmap::TOWNS)) {
            self.models.settlements.instances.replace(
                &self.gpu.device,
                &Instancer(&self.board).towns(TownKind::Settlement),
            );
            self.models.cities.instances.replace(
                &self.gpu.device,
                &Instancer(&self.board).towns(TownKind::City),
            );
        }

        if unlikely(self.board.has_modified(board::Dirtmap::ROBBER)) {
            self.models.robbers.instances.replace(
                &self.gpu.device,
                &Instancer(&self.board).robbers(),
            );
        }

        self.board.mark_clean();
    }
}
