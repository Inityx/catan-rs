pub mod piece;
pub mod tile;
pub mod defs;

use std::{array, collections::HashMap, iter};
use hex2d::{Coordinate, Direction, ToCoordinate};
use tile::*;
use crate::{
    board::defs::{DiceValue, Resource},
    geom::{
        spiral::InwardSpiral,
        bounds::Bounds,
        hour::Hour,
        located::Located,
        Location,
        CoordinateExt,
        shapes::{TILE_SPACING, STRAIGHT}, Rotation,
    },
};
use piece::Piece;
use cgmath::Zero;

mod count {
    pub const RINGS: usize = 3;
    pub const INTERIOR: usize = 19;
    pub const WATER: usize = 18;
    pub const HARBOR: usize = 9;
}

#[derive(Clone, Copy, Debug)]
pub struct Selection {
    pub coord: Coordinate<isize>,
    pub hour: Option<Hour>,
}

impl Selection {
    pub fn location(self) -> Location {
        self.coord.to_cartesian(TILE_SPACING) +
            self.hour.map(Hour::displacement).unwrap_or_else(Zero::zero)
    }

    pub fn rotation(self) -> Rotation {
        match self.hour {
            Some(hour) if hour.is_side() => -hour.rotation(),
            _ => STRAIGHT,
        }
    }
}

bitflags::bitflags! {
    pub struct Dirtmap: u8 {
        const TILES  = 0b0001;
        const ROADS  = 0b0010;
        const TOWNS  = 0b0100;
        const ROBBER = 0b1000;
    }
}

impl From<&Piece> for Dirtmap {
    fn from(piece: &Piece) -> Self {
        match piece {
            Piece::Town(_)   => Self::TOWNS,
            Piece::Road(_)   => Self::ROADS,
            Piece::Robber(_) => Self::ROBBER,
        }
    }
}

pub struct Board {
    tiles: HashMap<Coordinate<isize>, Tile>,
    bounds: Bounds,
    dirtmap: Dirtmap,
}

#[allow(dead_code)]
impl Board {
    pub fn standard_layout() -> Self {
        use {TileKind::*, Resource::*, Harbor::*, DiceValue::*};

        Board::standard_size(
            [
                Generic,
                Special(Lumber),
                Special(Brick),
                Generic,
                Generic,
                Special(Wool),
                Generic,
                Special(Ore),
                Special(Grain),
            ],
            [
                Land(Ore,    D10),
                Land(Grain,  D12),
                Land(Grain,  D9 ),
                Land(Lumber, D8 ),
                Land(Brick,  D5 ),
                Land(Grain,  D6 ),
                Land(Wool,   D11),
                Land(Wool,   D5 ),
                Land(Ore,    D8 ),
                Land(Brick,  D10),
                Land(Lumber, D9 ),
                Land(Wool,   D2 ),
                Land(Brick,  D6 ),
                Land(Lumber, D11),
                Land(Ore,    D3 ),
                Land(Grain,  D4 ),
                Land(Lumber, D3 ),
                Land(Wool,   D4 ),
                Desert,
            ],
        )
    }

    /// Create a board of standard size.
    ///
    /// 0 index is the top left tile, counter-clockwise (i.e. [`InwardSpiral`]).
    pub fn standard_size(
        harbors: [Harbor; count::HARBOR],
        interior: [TileKind; count::INTERIOR],
    ) -> Self {
        use itertools::Itertools;

        let water_border = harbors.iter().cloned()
            .map(Option::Some)
            .interleave_shortest(iter::repeat(Option::None))
            .map(TileKind::Water);

        let tiles: HashMap<_, _> = InwardSpiral::from_ring(count::RINGS)
            .zip(water_border
                .chain(array::IntoIter::new(interior))
                .map(Tile::from)
            )
            .collect();

        debug_assert_eq!(count::INTERIOR + count::WATER, tiles.len());

        let bounds = tiles.keys().cloned().fold(Bounds::INSIDE_OUT, Bounds::expand);

        Board { tiles, bounds, dirtmap: Dirtmap::empty() }
    }

    pub fn contains_tile(&self, at: Coordinate<isize>) -> bool { self.tiles.contains_key(&at) }

    pub fn tiles(&self) -> impl Iterator<Item=Located<&Tile>> { self.tiles.iter().map(Located::from) }

    pub fn bounds(&self) -> Bounds { self.bounds }

    pub fn producers(&self, rolled: DiceValue) -> impl '_ + Iterator<Item=Located<&Tile>> {
        self.tiles.iter()
            .filter(move |(_, tile)| tile.has_dice_value(rolled))
            .map(Located::from)
    }

    fn is_none_or_water(&self, coord: Coordinate<isize>) -> bool {
        self.tiles.get(&coord)
            .filter(|&tile| !tile.kind.is_water())
            .is_none()
    }

    fn probe_canonical_slot(&self, Selection { coord, hour }: Selection)
    -> Result<
        Option<(Coordinate<isize>, SlotDescriptor)>,
        Selection,
    > {
        use Hour::*;
        const BELOW:          Direction = Direction::ZY;
        const RIGHT_HIP:      Direction = Direction::XY;
        const RIGHT_SHOULDER: Direction = Direction::XZ;
        const LEFT_HIP:       Direction = Direction::ZX;
        const LEFT_SHOULDER:  Direction = Direction::YX;

        let hour = match hour {
            Some(hour) => hour,
            None => return Ok(
                // Center selection is always the robber of the same tile
                if self.tiles.contains_key(&coord) {
                    Some((coord, SlotDescriptor::Robber))
                } else {
                    None
                }
            ),
        };

        match hour {
            // Towns
            H7 | H9 => {
                let (intended_side, shoulder, hip) = match hour {
                    H7 => (TownSide::Right, RIGHT_SHOULDER, RIGHT_HIP),
                    H9 => (TownSide::Left,  LEFT_SHOULDER,  LEFT_HIP),
                    _ => unreachable!(),
                };

                // if not touching at least 1 land/desert, no selection
                if self.is_none_or_water(coord) &&
                    self.is_none_or_water(coord + BELOW.to_coordinate()) &&
                    self.is_none_or_water(coord + hip.to_coordinate())
                {
                    return Ok(None);
                }

                // If shoulder neighbor has adjacent town, no selection
                if let Some(shoulder_neighbor) = self.tiles.get(&(coord + shoulder.to_coordinate())) {
                    if matches!(
                        shoulder_neighbor.town,
                        (Some(_), side) if -side == intended_side
                    ) {
                        return Ok(None);
                    }
                }

                // If hip neighbor has adjacent town, no selection
                if let Some(hip_neighbor) = self.tiles.get(&(coord + hip.to_coordinate())) {
                    if matches!(
                        hip_neighbor.town,
                        (Some(_), side) if -side == intended_side
                    ) {
                        return Ok(None);
                    }
                }

                let selection = match self.tiles.get(&coord).unwrap().town {
                    (Some(_), side) if side != intended_side => None,
                    _ => Some((coord, SlotDescriptor::Town(intended_side))),
                };

                Ok(selection)
            },
            // Roads
            H6 | H8 | H10 => {
                let (index, hip) = match hour {
                    H6  => (RoadIndex::R0, RIGHT_HIP),
                    H8  => (RoadIndex::R1, BELOW    ),
                    H10 => (RoadIndex::R2, LEFT_HIP ),
                    _ => unreachable!(),
                };

                // if not touching at least 1 land/desert, no selection
                let ineligible = self.is_none_or_water(coord) && self.is_none_or_water(coord + hip.to_coordinate());

                let selection = if ineligible { None } else { Some((coord, SlotDescriptor::Road(index))) };

                Ok(selection)
            },
            // selections owned by a different tile
            _ => {
                let new_direction = match hour {
                    H1 | H2 | H3 => Direction::YZ,
                    H4 | H5      => Direction::XZ,
                    H11 | H12    => Direction::YX,
                    _ => unreachable!(),
                };

                let new_hour = match hour {
                    H1  => H9,
                    H2  => H8,
                    H3  => H7,
                    H4  => H10,
                    H5  => H9,
                    H11 => H7,
                    H12 => H6,
                    _ => unreachable!(),
                };

                Err(Selection {
                    coord: coord + new_direction.to_coordinate(),
                    hour: Some(new_hour),
                })
            },
        }
    }

    fn canonical_slot(&self, at: Selection) -> Option<(Coordinate<isize>, SlotDescriptor)> {
        self.probe_canonical_slot(at)
            .unwrap_or_else(|correction| self.canonical_slot(correction))
    }

    pub fn get(&self, at: Selection) -> Option<Piece> {
        let (coord, descriptor) = self.canonical_slot(at)?;

        self.tiles.get(&coord).unwrap()
            .slot_data(descriptor).into_piece()
    }

    pub fn try_put(&mut self, piece: impl Into<Piece>, at: Selection) -> Result<(), ()> {
        let piece = piece.into();

        let (coord, descriptor) = self.canonical_slot(at).ok_or(())?;

        let dirty_flag = Dirtmap::from(&piece);

        self.tiles.get_mut(&coord).unwrap()
            .slot_ref(descriptor)
            .try_fill(piece)?;

        self.dirtmap.insert(dirty_flag);

        Ok(())
    }

    pub fn erase(&mut self, at: Selection) {
        let prev_piece = self.canonical_slot(at)
            .map(|(coord, descriptor)| self.tiles.get_mut(&coord).unwrap().slot_ref(descriptor))
            .and_then(|mut slot| slot.remove());

        if let Some(piece) = prev_piece {
            self.dirtmap.insert(Dirtmap::from(&piece));
        }
    }

    pub fn has_modified(&self, flag: Dirtmap) -> bool { self.dirtmap.contains(flag) }
    pub fn mark_clean(&mut self) { self.dirtmap = Dirtmap::empty(); }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn default() {
        let board = Board::standard_layout();
        assert_eq!(37, board.tiles.len());
    }

    mod selection {
        use super::super::*;

        lazy_static::lazy_static! {
            static ref BOARD: Board = Board::standard_layout();
        }

        #[test]
        fn center() {
            for (x, y) in vec![(0, 0), (1, 1), (-1, 1)] {
                assert_eq!(
                    Some((Coordinate { x, y }, SlotDescriptor::Robber)),
                    BOARD.canonical_slot(Selection { coord: Coordinate { x, y }, hour: None }),
                );
            }
        }

        #[test]
        fn owned_roads() {
            let coord = Coordinate::new(0, 0);

            for (hour, index) in vec![
                (Hour::H6, RoadIndex::R0),
                (Hour::H8, RoadIndex::R1),
                (Hour::H10, RoadIndex::R2),
            ] {
                let correct = Some((coord, SlotDescriptor::Road(index)));
                let computed = BOARD.canonical_slot(Selection { coord, hour: Some(hour) });
                assert_eq!(
                    correct, computed,
                    "Selection at {:?} should have been {:?}, but was {:?}",
                    (coord, hour), correct, computed,
                );
            }
        }

        #[test]
        fn owned_towns() {
            let coord = Coordinate::new(0, 0);

            for (hour, side) in vec![
                (Hour::H7, TownSide::Right),
                (Hour::H9, TownSide::Left),
            ] {
                let correct = Some((coord, SlotDescriptor::Town(side)));
                let computed = BOARD.canonical_slot(Selection { coord, hour: Some(hour) });
                assert_eq!(
                    correct, computed,
                    "Selection at {:?} should have been {:?}, but was {:?}",
                    (coord, hour), correct, computed,
                );
            }
        }
    }
}
