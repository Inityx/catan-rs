use futures::executor::block_on;
use winit::{window::Window, dpi::PhysicalSize};
use wgpu::*;
use std::{ops::Range, io::{self, Cursor as IoCursor}};

use crate::geom::Location;

pub mod frame;
pub mod buffer;

use buffer::{
    objects::{Instance, Uniforms},
    descriptor_traits::{VertexBufferItem, BindGroupItem},
};

pub type DrawRange = Range<u32>;

mod config {
    use super::*;

    pub const MSAA_SAMPLES: u32 = 1;
    pub const DEPTH_STENCIL_STATE: Option<DepthStencilStateDescriptor> = None;
    pub const PRIMITIVE_TOPOLOGY: PrimitiveTopology = PrimitiveTopology::TriangleList;
    pub const SAMPLE_MASK: u32 = u32::MAX;
    pub const ALPHA_TO_COVERAGE: bool = false;

    pub const DEVICE_DESCRIPTOR: DeviceDescriptor = DeviceDescriptor {
        extensions: Extensions { anisotropic_filtering: false },
        limits: Limits { max_bind_groups: MAX_BIND_GROUPS as u32 },
    };

    pub const VERT_SHADER: &[u8] = include_bytes!("../../../assets/build/shaders/shader.vert.spv");
    pub const FRAG_SHADER: &[u8] = include_bytes!("../../../assets/build/shaders/shader.frag.spv");

    pub const RAST_STATE_DESC: Option<RasterizationStateDescriptor> = Some(RasterizationStateDescriptor {
        front_face: FrontFace::Ccw,
        cull_mode: CullMode::None,
        depth_bias: 0,
        depth_bias_slope_scale: 0.0,
        depth_bias_clamp: 0.0,
    });

    pub const MIX: BlendDescriptor = BlendDescriptor {
        src_factor: BlendFactor::SrcAlpha,
        dst_factor: BlendFactor::OneMinusSrcAlpha,
        operation: BlendOperation::Add,
    };

    pub const COLOR_STATES: &[ColorStateDescriptor] = &[ColorStateDescriptor {
        format: TextureFormat::Bgra8UnormSrgb,
        color_blend: MIX,
        alpha_blend: MIX,
        write_mask: ColorWrite::ALL,
    }];

    pub const VERTEX_STATE: VertexStateDescriptor = VertexStateDescriptor {
        index_format: IndexFormat::Uint16,
        vertex_buffers: &[
            <Location as VertexBufferItem<0, {InputStepMode::Vertex  }>>::BUFFER_DESCRIPTOR,
            <Instance as VertexBufferItem<1, {InputStepMode::Instance}>>::BUFFER_DESCRIPTOR,
        ],
    };

    pub const fn swap_chain_desc(PhysicalSize { width, height }: PhysicalSize<u32>) -> SwapChainDescriptor {
        SwapChainDescriptor {
            usage: TextureUsage::OUTPUT_ATTACHMENT,
            format: TextureFormat::Bgra8UnormSrgb,
            width,
            height,
            present_mode: PresentMode::Mailbox,
        }
    }
}

pub struct Gpu {
    pub device: Device,
    surface: Surface,
    queue: Queue,
    pub uniform_bind_group_layout: BindGroupLayout,
    pub render_pipeline: RenderPipeline,
    swap_chain: SwapChain,
}

impl Gpu {
    pub fn new(window: &Window) -> io::Result<Self> {
        let surface = Surface::create(window);

        let adapter = block_on(Adapter::request(
            &RequestAdapterOptions {
                power_preference: PowerPreference::Default,
                compatible_surface: Some(&surface),
            },
            BackendBit::PRIMARY,
        )).ok_or_else(||
            io::Error::new(io::ErrorKind::NotFound, "No appropriate graphics adapter found")
        )?;

        let (device, queue) = block_on(adapter.request_device(&config::DEVICE_DESCRIPTOR));

        let vs_module = device.create_shader_module(&read_spirv(IoCursor::new(config::VERT_SHADER))?);
        let fs_module = device.create_shader_module(&read_spirv(IoCursor::new(config::FRAG_SHADER))?);

        let vertex_stage   =      ProgrammableStageDescriptor { module: &vs_module, entry_point: "main" };
        let fragment_stage = Some(ProgrammableStageDescriptor { module: &fs_module, entry_point: "main" });

        let uniform_bind_group_layout = device.create_bind_group_layout(&Uniforms::BIND_GROUP_LAYOUT_DESCRIPTOR);

        let pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            bind_group_layouts: &[&uniform_bind_group_layout],
        });

        let render_pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
            layout: &pipeline_layout,
            vertex_stage,
            fragment_stage,
            rasterization_state: config::RAST_STATE_DESC,
            primitive_topology: config::PRIMITIVE_TOPOLOGY,
            color_states: config::COLOR_STATES,
            vertex_state: config::VERTEX_STATE,
            depth_stencil_state: config::DEPTH_STENCIL_STATE,
            sample_count: config::MSAA_SAMPLES,
            sample_mask: config::SAMPLE_MASK,
            alpha_to_coverage_enabled: config::ALPHA_TO_COVERAGE,
        });

        let swap_chain = device.create_swap_chain(&surface, &config::swap_chain_desc(window.inner_size()));

        Ok(Self {
            device,
            surface,
            queue,
            uniform_bind_group_layout,
            render_pipeline,
            swap_chain,
        })
    }

    pub fn resize(&mut self, size: PhysicalSize<u32>) {
        self.swap_chain = self.device.create_swap_chain(
            &self.surface,
            &config::swap_chain_desc(size),
        )
    }

    pub fn next_frame(&mut self) -> SwapChainOutput {
        self.swap_chain
            .get_next_texture()
            .expect("Timeout acquiring next SwapChain texture")
    }

    pub fn bind_group(&self, layout: &BindGroupLayout, bindings: &[Binding]) -> BindGroup {
        self.device.create_bind_group(&BindGroupDescriptor {
            layout,
            bindings,
            label: None,
        })
    }

    pub fn command_encoder(&self) -> CommandEncoder {
        self.device.create_command_encoder(&CommandEncoderDescriptor { label: None })
    }

    pub fn submit(&self, command_buffer: CommandBuffer) {
        self.queue.submit(&[command_buffer])
    }
}
